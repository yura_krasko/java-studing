package org.hillel_Elem.homework1;

import static java.lang.System.*;

public class ConditionalStatements {

    /**
     *Search max of two variables
     *
     * @param a
     * @param b
     */
    public static Integer maxElemOfTwoNumbers(Integer a, Integer b){

        if (a > b){

            return a;
        } else{

            return b;
        }
    }

    /**
     *Search max of three variables
     *
     * @param a
     * @param b
     * @param c
     */
    public  static Integer maxElmOfThreeNumbers(Integer a, Integer b, Integer c){

      Integer max = maxElemOfTwoNumbers(a,b);

      if (max > c){

          return max;
      } else {

          return c;
      }
    }

    /**
     *Search max of four variables
     *
     * @param a
     * @param b
     * @param c
     * @param d
     */

    public static Integer maxElemOfFourNumbers(Integer a, Integer b, Integer c, Integer d){

        Integer max = maxElmOfThreeNumbers(a, b, c);

        if (max > d){

            return max;
        } else{

            return d;
        }
    }

    /**
     * Search event number
     *
     * @param a
     * @param b
     */
    public static void eventNumber(Integer a, Integer b){

        if (a % 2 == 0){

            out.println("a event, rez = " + (a * b));
        } else{

            out.println("a not event, rez = " + (a + b));
        }
    }


    /**
     * Sort two variables ascending
     *
     * @param a
     * @param b
     */
    public static  void sortTwoNumbers(int a, int b){

        if (a > b){

            out.println(b + " " + a);
        } else{

            out.println(a + " " + b);
        }
    }


    /**
     *Sort three variables ascending
     *
     * @param a
     * @param b
     * @param c
     */
    public static void sortThreeNumbers(Integer a, Integer b, Integer c){

        Integer max, min;

        if (a > b){

            max = a;
            min = b;
        } else{

            max = b;
            min = a;
        }

        if (min > c && max > c ){

            out.println(c + " " + min + " " + max);
        }
        else if (max > c && c > min){

            out.println(min + " " + c + " " + max);
        } else{

            out.println(min + " " + max + " " + c);
        }
    }

    /**
     *Sort four variables ascending
     *
     * @param a
     * @param b
     * @param c
     * @param d
     */
    public static  void sortForthNumbers (int a, int b, int c, int d) {

       int max, max1, min, min1;

       if (a > b){

           max = a;
           min = b;
       } else{

           max = b;
           min = a;
       }

       if (c > d){

           max1 = c;
           min1 = d;
       } else{

           max1 = d;
           min1 = c;
       }

       if(max > max1 && max > min1){
           if (min > max1 && min > min1){

               out.println(min1 + " " + max1 + " " + min + " " + max);
           }
           else if (min < max1 && min > min1){

               out.println(min1 + " " + min + " " + max1 + " " + max);
           }
           else if (min < max1 && min < min1){

               out.println(min + " " + min1 + " " + max1 + " " + max);
           } else{

               out.println(min + " " + max1 + " " + min + " " + max);
           }
       }

       else if (max > max1 && max < min1){

           if (min > max1 && min > min1){

               out.println(min1 + " " + max1 + " " + min + " " + max);
           }
           else if (min < max1 && min > min1){

               out.println(min1 + " " + min + " " + max1 + " " + max);
           }
           else if (min < max1 && min < min1){

               out.println(min + " " + min1 + " " + max1 + " " + max);
           } else{

               out.println(min + " " + max1 + " " + min + " " + max);
           }
       }

       else if (max1 > max && max1 < min){

           if (min1 > max && min1 > min){

               out.println(min + " " + max + " " + min1 + " " + max1);
           }
           else if (min1 < max && min1 > min){

               out.println(min + " " + min1 + " " + max + " " + max1);
           }
           else if (min1 < max && min1 < min){

               out.println(min1 + " " + min + " " + max + " " + max1);
           } else{

               out.println(min1 + " " + max + " " + min1 + " " + max1);
           }

       } else {
           if (min1 > max && min1 > min){

               out.println(min + " " + max + " " + min1 + " " + max1);
           }
           else if (min1 < max && min1 > min){

               out.println(min + " " + min1 + " " + max + " " + max1);
           }
           else if (min1 < max && min1 < min){

               out.println(min1 + " " + min + " " + max + " " + max1);
           } else{

               out.println(min1 + " " + max + " " + min1 + " " + max1);
           }
       }
    }
}
