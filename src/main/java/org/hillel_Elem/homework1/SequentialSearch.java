package org.hillel_Elem.homework1;


public class SequentialSearch {

    /**
     * Search element in array
     *
     * @param array
     * @param b
     */
    public static <T> String seqSearchElem(T [] array, T b) {

        for (T i : array)
            if (i == b) {
                return "Array contains " + b;
        }

        return "Array does not contain " + b;
    }

}
