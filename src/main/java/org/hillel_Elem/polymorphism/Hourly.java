package org.hillel_Elem.polymorphism;

public class Hourly extends Employee {

    private int hoursWorked;

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Hourly(String eName, String eAddress, String ePhone,String socSecNumber,  double rate, double hourCon, double hourDoc){

        super(eName, eAddress, ePhone, socSecNumber, rate, hourCon, hourDoc);
        hoursWorked = 0;
    }

    /**
     *
     * @param moreHours
     */
    public  void addHours(int moreHours){
        hoursWorked += moreHours;
    }

    /**
     *
     * @return
     */
    @Override
    public double pay(){

        double payment = (0.9 * super.pay()) + hoursWorked;
        return payment;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nCurrent hours: " + hoursWorked;
        return  result;

    }

    /**
     *
     * @return
     */
    @Override
    public  double payConsultation(){

        return  (0.9 * super.payConsultation()) + hoursWorked;
    }

    /**
     *
     * @return
     */
    @Override
    public double payDocumentation(){

        return super.payDocumentation();
    }

    /**
     *
     * @return
     */
    @Override
    public double payDevelopment(){

        return pay() - payConsultation() - payDocumentation();
    }
}
