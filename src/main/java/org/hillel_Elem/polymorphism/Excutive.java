package org.hillel_Elem.polymorphism;

public class Excutive extends Employee {

    private double bonus;

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socsecNumber
     * @param rate
     */
    public Excutive(String eName, String eAddress, String ePhone, String socsecNumber, double rate, double hourCon, double houDoc){

        super(eName, eAddress, ePhone, socsecNumber, rate, hourCon, houDoc);
        bonus = 0;
    }



    /**
     *
     * @param execBonus
     */
    public  void awardBonus (double execBonus){

    bonus = execBonus;
    }

    /**
     *
     * @return
     */
    @Override
    public  double pay(){

        double payment = super.pay() + bonus;
        bonus = 0.0;
        return payment;
    }

    /**
     *
     *
     * @return
     */
    @Override
    public double payConsultation(){

        return super.payConsultation();
    }

    /**
     *
     * @return
     */
    @Override
    public double payDocumentation(){

        return super.payDocumentation() * 1.1;
    }

    /**
     *
     * @return
     */
    @Override
    public double payDevelopment(){

        return pay() - payConsultation() - payDocumentation();
    }
}
