package org.hillel_Elem.polymorphism;

import java.util.Comparator;

public class Employee extends StaffMember {

    private String socialSucurityNumber;
    private double payRate;
    private double countHoursConsultation;
    private double countHoursDocumentation;

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecNumber
     * @param rate
     * @param hourCon
     * @param hourDoc
     */
    public Employee(String eName, String eAddress, String ePhone, String socSecNumber, double rate, double hourCon, double hourDoc){
        super(eName, eAddress, ePhone);
        socialSucurityNumber = socSecNumber;
        payRate = rate;
        countHoursConsultation = hourCon;
        countHoursDocumentation = hourDoc;


    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {

        String rezult = super.toString();
        rezult +="\nSocial Securitu Number: " + socialSucurityNumber;
        return rezult;
    }

    /**
     *
     * @return
     */
    @Override
    public double pay(){

        return payRate;
    }

    /**
     *
     * @return
     */
    @Override
    public double payConsultation() {
        return 0.1 * payRate * countHoursConsultation;
    }

    /**
     *
     * @return
     */
    @Override
    public double payDocumentation() {
        return 0.07 * payRate * countHoursDocumentation ;
    }

    /**
     *
     * @return
     */
    @Override
    public double payDevelopment() {
        return pay() - payConsultation() - payDocumentation();
    }


}
