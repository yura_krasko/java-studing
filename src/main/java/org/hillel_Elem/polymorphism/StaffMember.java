package org.hillel_Elem.polymorphism;

import java.util.Comparator;

abstract public class StaffMember {

    protected  String  name;
    protected String address;
    protected String phone;

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    public StaffMember(String eName, String eAddress, String ePhone){

        name = eName;
        address = eAddress;
        phone = ePhone;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String rezult = "Name: " + name + "\n";
        rezult += "Address: "  + address + "\n";
        rezult += "Phone: " +phone + "\n";
        return rezult;
    }

    /**
     *
     * @return
     */
    public abstract double pay();

    /**
     *
     * @return
     */
    public abstract double payConsultation();

    /**
     *
     * @return
     */
    public abstract  double payDocumentation();

    /**
     *
     * @return
     */
    public abstract double payDevelopment();

    /**
     *Sort by pay
     */
    public static final Comparator<StaffMember> payComparator = new Comparator<StaffMember>() {
        @Override
        public int compare(StaffMember o1, StaffMember o2) {
            return (int) (o1.pay() - o2.pay());
        }
    };

    /**
     *Sort by payConsultation
     */
    public static final  Comparator<StaffMember> payConsComparator = new Comparator<StaffMember>() {
        @Override
        public int compare(StaffMember o1, StaffMember o2) {
            return (int) (o1.payConsultation() - o2.payConsultation());
        }
    };

    /**
     *Sort by payDocumentation
     */
    public static final  Comparator<StaffMember> payDocComparator = new Comparator<StaffMember>() {
        @Override
        public int compare(StaffMember o1, StaffMember o2) {
            return (int) (o1.payDocumentation() - o2.payDocumentation());
        }
    };


    /**
     *Sort by payDevelopment
     */
    public static final  Comparator<StaffMember> payDevComparator = new Comparator<StaffMember>() {
        @Override
        public int compare(StaffMember o1, StaffMember o2) {
            return (int) (o1.payDevelopment() - o2.payDevelopment());
        }
    };


}
