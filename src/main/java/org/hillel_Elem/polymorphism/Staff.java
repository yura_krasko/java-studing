package org.hillel_Elem.polymorphism;

import java.util.ArrayList;

public class Staff {

    ArrayList<StaffMember> staffList;

    /**
     *
     */
    public Staff(){

        staffList = new ArrayList<>();
        staffList.add(new Excutive("Sam", "123 Main Line", "555-0469", "123-45-678", 2423.07, 1.0 , 1.4));
        staffList.add(new Employee("Carla", "456 off Line", "555-0000", "010-20-3040", 1245.25, 1.0, 1.5));
        staffList.add(new Employee("Woody", "789 off Rocket", "555-0690", "958-47-3625", 10.55, 1.0, 1.4));
        staffList.add(new Hourly("Diane", "675 Firs Ave", "555-6542", "958-47-765", 1245.25, 1.0, 2.0));
        staffList.add(new Volonteer("Norm", "987 Suds Blvd", "555-5456"));
        staffList.add(new Volonteer("Cliff", "321 Duds Line", "555-6543"));

        ((Excutive)staffList.get(0)).awardBonus(15);
        ((Hourly)staffList.get(3)).addHours(10);
    }

    /**
     *
     */
    public void payday(){

        double amount;
        double amountConsultation;
        double amountDocumentation;
        double amountDevelopment;

        for(int count = 0; count < staffList.size(); count++){

            System.out.println(staffList.get(count));
            amount = staffList.get(count).pay();
            amountConsultation = staffList.get(count).payConsultation();
            amountDocumentation = staffList.get(count).payDocumentation();
            amountDevelopment = staffList.get(count).payDevelopment();


            if(amount == 0.0 && amountConsultation == 0.0 && amountDocumentation == 0.0
                    && amountDevelopment == 0.0){
                System.out.println("Paid: Thanks!");
                System.out.println("Paid behind consultation: Thanks!");
                System.out.println("Paid behind documentation: Thanks!" );
                System.out.println("Paid behind development: Thanks!" );
            } else{
                System.out.println("Paid: " + amount);
                System.out.println("Paid behind consultation: " + amountConsultation);
                System.out.println("Paid behind dcumentation: " + amountDocumentation);
                System.out.println("Paid behind development: " + amountDevelopment);
            }

            System.out.println("-------------------------------------------");

        }
    }
}
