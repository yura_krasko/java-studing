package org.hillel_Elem.polymorphism;

public class Volonteer extends StaffMember  {

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    public Volonteer(String eName, String eAddress, String ePhone){
        super(eName, eAddress, ePhone);
    }

    /**
     *
     * @return
     */
    @Override
    public  double pay(){
        return 0.0;
    }

    /**
     *
     * @return
     */
    @Override
    public double payConsultation() {
        return 0.0;
    }

    /**
     *
     * @return
     */
    @Override
    public double payDocumentation() {
        return 0.0;
    }

    /**
     *
     * @return
     */
    @Override
    public double payDevelopment() {
        return 0.0;
    }


}
