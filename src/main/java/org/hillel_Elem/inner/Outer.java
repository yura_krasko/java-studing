package org.hillel_Elem.inner;

import java.sql.Struct;

public class Outer {

    private static String textNested = "Nested String";
    private String text = "I am private";

    /**
     *git status
     *
     */
    public static class Nested{

        public  void printNestedtext(){
            System.out.println(textNested);

        }
    }


    public class Inner{

        private  String text = "I am inner private";

        /**
         *
         */
        public void printText(){

            System.out.println(text);
            System.out.println(Outer.this.text);
        }
    }

    public  void test(){

    }

    public void local(){

        class Local{

        }

        Local local = new Local();
    }

    public void doIt(){

        System.out.println("OuterClass doIt()");
    }
}
