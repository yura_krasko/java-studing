package org.hillel_Elem.inner;

public class LocalInner {

    private String message = "yura.com";

    void display(){

        final  int DATA = 20;

        class Local{

            void msg(){

                System.out.println(message + " : " + DATA);
            }
        }

        Local loc = new Local();
        loc.msg();

    }
}
