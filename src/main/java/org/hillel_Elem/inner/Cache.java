package org.hillel_Elem.inner;

import com.sun.deploy.cache.CacheEntry;

import java.util.HashMap;
import java.util.Map;

public class Cache {

    private Map<String, CacheEntry> cacheMap = new HashMap<>();

    private class CacheEntry{

        public Long timeInserted = 0l;
        public Object value = null;
    }

    /**
     *
     * @param key
     * @param value
     */
    public void store(String key, Object value){

        CacheEntry entry = new CacheEntry();
        entry.value = value;
        entry.timeInserted = System.currentTimeMillis();
        this.cacheMap.put(key, entry);

    }

    /**
     *
     * @param key
     * @return
     */
    public Object get(String key){

        CacheEntry entry = this.cacheMap.get(key);
        if(entry == null){
            return null;
        }
        return entry.value;
    }

    /**
     *
     * @param key
     * @return
     */
    public String getData(String key){

        CacheEntry entry = this.cacheMap.get(key);
        if(entry == null){
            return null;
        }

        return entry.value.toString().concat(" ").concat(entry.timeInserted.toString());
    }
}
