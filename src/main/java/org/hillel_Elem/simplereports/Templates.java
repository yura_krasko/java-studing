package org.hillel_Elem.simplereports;

import net.sf.dynamicreports.report.builder.ReportTemplateBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.constant.VerticalTextAlignment;

import java.awt.*;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

public class Templates {

    public static final StyleBuilder rootStyle;
    public static final StyleBuilder columnStyle;
    public static final StyleBuilder boldStyle;
    public static final StyleBuilder boldCenteredStyle;
    public static final ReportTemplateBuilder reportTemplate;
    public static final StyleBuilder columnTitleStyle;


    static{

        rootStyle           = stl.style().setPadding(5);
        boldStyle = stl.style().bold();
        boldCenteredStyle = stl.style(boldStyle).setTextAlignment(HorizontalTextAlignment.CENTER, VerticalTextAlignment.MIDDLE);
        columnStyle         = stl.style(rootStyle).setVerticalTextAlignment(VerticalTextAlignment.MIDDLE);

        reportTemplate = template()
                .highlightDetailEvenRows();

        columnTitleStyle    = stl.style(columnStyle)
                .setBorder(stl.pen1Point())
                .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                .setBackgroundColor(Color.LIGHT_GRAY)
                .bold();

    }

}
