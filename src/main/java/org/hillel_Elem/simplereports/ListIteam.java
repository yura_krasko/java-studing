package org.hillel_Elem.simplereports;



import org.hillel_Elem.reports.Invoice;
import org.hillel_Elem.reports.Item;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ListIteam {

    List<ItemReport> iteams;

    public List<ItemReport> getIteams() {
        return iteams;
    }

    public void setIteams(List<ItemReport> iteams) {
        this.iteams = iteams;
    }

    private Invoice createInvoice(){

        Invoice invoice = new Invoice();

        List<Item> iteams = new ArrayList<>();
        iteams.add(createIteam("Notebook", 2, new BigDecimal(3000)));
        iteams.add(createIteam("DVD", 6, new BigDecimal(56733)));
        iteams.add(createIteam("TV", 3, new BigDecimal(8734)));

        return invoice;

    }

    private Item createIteam(String description, Integer count, BigDecimal price){

        Item item = new Item();
        item.setDescription(description);

        return item;

    }
}
