package org.hillel_Elem.simplereports;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ListDataReport {

    private  ListIteam listItem;

    public ListDataReport() {

        listItem = createList();
    }

    private ItemReport createIteamReport(String description, Integer count, BigDecimal price){

        ItemReport item = new ItemReport();
        item.setDescription(description);
        item.setCount(count);
        item.setPrice(price);

        return item;
    }

    private ListIteam createList(){

        ListIteam  iteams = new ListIteam ();

        List<ItemReport> list = new ArrayList<>();
        list.add(createIteamReport("Notebook", 2, new BigDecimal(1000)));
        list.add(createIteamReport("DVD",3, new BigDecimal(655)));
        list.add(createIteamReport("TV", 8, new BigDecimal(9855)));
        list.add(createIteamReport("Book", 4, new BigDecimal(855)));
        list.add(createIteamReport("PC", 9, new BigDecimal(19855)));
        list.add(createIteamReport("Telephone", 54, new BigDecimal(976855)));
        list.add(createIteamReport("Router", 81, new BigDecimal(4316)));
        iteams.setIteams(list);

        return iteams;
    }

    public ListIteam getListItem() {
        return listItem;
    }

    public JRDataSource createDataSource(){

        return new JRBeanCollectionDataSource(listItem.getIteams());
    }
}
