package org.hillel_Elem.simplereports;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;


import java.math.BigDecimal;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

public class ListDesigneReport {

    private ListDataReport data = new ListDataReport();

    public ListDesigneReport() {
    }

    public ListDesigneReport(ListDataReport data) {
        this.data = data;
    }


    public JasperReportBuilder build() throws Exception{

        JasperReportBuilder report = report();

        TextColumnBuilder<String> descriptionColumn = col.column("Description", "description", type.stringType())
                .setFixedWidth(200);
        TextColumnBuilder<Integer> countColumn = col.column("Count", "count", type.integerType())
                .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        TextColumnBuilder<BigDecimal> priceColumn = col.column("Price", "price", type.bigDecimalType());

        report
                .setColumnTitleStyle(Templates.columnTitleStyle)
                .setTemplate(Templates.reportTemplate)
                .columns(descriptionColumn, countColumn, priceColumn)
                .title(cmp.text("Test Report").setStyle(Templates.boldCenteredStyle))
                .setDataSource(data.createDataSource());

        return report;
    }
}
