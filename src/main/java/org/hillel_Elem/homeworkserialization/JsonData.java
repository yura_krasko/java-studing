package org.hillel_Elem.homeworkserialization;

import org.hillel_Elem.homeworkserialization.model.Address;
import org.hillel_Elem.homeworkserialization.model.Item;
import org.hillel_Elem.homeworkserialization.model.Order;
import org.hillel_Elem.homeworkserialization.model.User;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JsonData{


    /**
     * Data to create json
     *
     * @return
     */
    public Order orderJson(){

        Order order = new Order();
        order.setId(1);
        order.setNumberOrder(10002576);
        order.setDateOrder(createDate());
        order.setUser(createUser("Will", "Smith", 43, "+38093123456"));
        order.setAddress(createAddress("New York", "Washington Street 54", 12345));

        List<Item> itemList = new ArrayList<>();
        itemList.add(createIteam("Phone", 2, new BigDecimal(3000)));
        itemList.add(createIteam("DVD", 4, new BigDecimal(2333)));

        order.setIteams(itemList);
        return order;
    }

    /**
     * Create User
     *
     * @param firstName
     * @param lastName
     * @param age
     * @param phone
     * @return
     */
    private User createUser(String firstName, String lastName, Integer age, String phone) {

        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setAge(age);
        user.setPhone(phone);

        return user;
    }

    /**
     * Create Address
     *
     * @param city
     * @param streetAddress
     * @param postCode
     * @return
     */
    private Address createAddress(String city, String streetAddress, Integer postCode) {

        Address address = new Address();
        address.setCity(city);
        address.setStreetAddress(streetAddress);
        address.setPostCode(postCode);

        return address;
    }

    /**
     * Create Item
     *
     * @param description
     * @param quantity
     * @param price
     * @return
     */
    private Item createIteam(String description, Integer quantity, BigDecimal price) {

        Item item = new Item();
        item.setDescription(description);
        item.setQuantity(quantity);
        item.setPrice(price);

        return item;
    }

    /**
     * Create Date
     *
     * @return
     */
    private String createDate(){

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date();

        return dateFormat.format(date);
    }
}
