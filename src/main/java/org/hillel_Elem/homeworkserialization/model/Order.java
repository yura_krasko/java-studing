package org.hillel_Elem.homeworkserialization.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class Order  {

    private static final long serialVersionUID = 13423423L;

    private Integer id;
    private Integer numberOrder;
    private String dateOrder;
    private User user;
    private Address address;
    private List<Item> iteams;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumberOrder() {
        return numberOrder;
    }

    public void setNumberOrder(Integer numberOrder) {
        this.numberOrder = numberOrder;
    }

    public String getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(String dateOrder) {
        this.dateOrder = dateOrder;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Item> getIteams() {
        return iteams;
    }

    public void setIteams(List<Item> iteams) {
        this.iteams = iteams;
    }
}
