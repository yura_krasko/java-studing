package org.hillel_Elem.homeworkserialization.model;

import java.io.Serializable;

public class User implements Serializable{

    private static final long serialVersionUID = 12312443223L;

    private String firstName;
    private String lastName;
    private Integer age;
    private String phone;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
