package org.hillel_Elem.homeworkserialization.model;

import java.io.Serializable;

public class Address implements Serializable {

    private static final long serialVersionUID = 12345566L;

    private String city;
    private String streetAddress;
    private Integer postCode;


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public Integer getPostCode() {
        return postCode;
    }

    public void setPostCode(Integer postCode) {
        this.postCode = postCode;
    }
}


