package org.hillel_Elem.homeworkserialization.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Item implements Serializable{

    private static final long serialVersionUID = 43242342L;

    private String description;
    private Integer quantity;
    private BigDecimal price;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
