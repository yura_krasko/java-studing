package org.hillel_Elem.homeworkpatterns.abstractfactory;

public class Nissan implements Car {
    @Override
    public Car driving() {
        System.out.println("create Nissan");
        return null;
    }
}
