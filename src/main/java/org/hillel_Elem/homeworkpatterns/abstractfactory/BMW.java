package org.hillel_Elem.homeworkpatterns.abstractfactory;

public class BMW implements Car {
    @Override
    public Car driving() {
        System.out.println("create BMW");
        return null;
    }
}
