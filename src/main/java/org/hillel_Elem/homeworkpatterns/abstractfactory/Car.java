package org.hillel_Elem.homeworkpatterns.abstractfactory;

public interface Car {

    Car driving();
}
