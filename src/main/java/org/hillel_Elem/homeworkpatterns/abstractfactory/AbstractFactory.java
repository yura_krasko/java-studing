package org.hillel_Elem.homeworkpatterns.abstractfactory;

public class AbstractFactory{

    public Factory getFactory(String typeOfFactory) {
        if(typeOfFactory.equals("German")){

            return  new GermanyFactory();
        } else {

            return  new JapanFactory();
        }
    }
}
