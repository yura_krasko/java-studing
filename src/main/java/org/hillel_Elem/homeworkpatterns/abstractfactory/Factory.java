package org.hillel_Elem.homeworkpatterns.abstractfactory;

public interface Factory {

    Car getFactory(String typeOfFactory);
}
