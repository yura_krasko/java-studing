package org.hillel_Elem.homeworkpatterns.abstractfactory;

public class JapanFactory implements Factory{

    @Override
    public Car getFactory(String typeOfFactory) {
        if (typeOfFactory.equals("Toyota")){

            return  new Toyota();
        }   else {

            return new Nissan();
        }
    }
}
