package org.hillel_Elem.homeworkpatterns.abstractfactory;

public class Audi implements Car {

    @Override
    public Car driving() {
        System.out.println("create Audi");
        return null;
    }
}
