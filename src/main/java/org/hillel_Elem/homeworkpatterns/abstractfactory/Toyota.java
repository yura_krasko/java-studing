package org.hillel_Elem.homeworkpatterns.abstractfactory;

public class Toyota implements Car {

    @Override
    public Car driving() {
        System.out.println("create Toyota");
        return null;
    }
}
