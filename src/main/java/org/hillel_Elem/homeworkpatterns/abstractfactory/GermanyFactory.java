package org.hillel_Elem.homeworkpatterns.abstractfactory;

public class GermanyFactory implements Factory {
    @Override
    public Car getFactory(String typeOfFactory) {
        if (typeOfFactory.equals("BMW")){

            return  new BMW();
        } else {

            return  new Audi();
        }
    }
}
