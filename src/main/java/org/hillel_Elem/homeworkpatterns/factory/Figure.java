package org.hillel_Elem.homeworkpatterns.factory;

public interface Figure {

    Double getArea();
    String darw();
}
