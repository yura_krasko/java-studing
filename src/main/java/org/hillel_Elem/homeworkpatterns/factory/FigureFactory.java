package org.hillel_Elem.homeworkpatterns.factory;

public class FigureFactory {

    public Figure getFigure( String typeOfFigure){

        switch (typeOfFigure){
            case "Circle" : return new Circle();
            case "Cylinder" : return new Cylinder();
            default : return null;
        }
    }
}
