package org.hillel_Elem.homeworkpatterns.factory;

public class Cylinder implements Figure{

    private Double height = 5.0;

    Circle circle = new Circle();


    @Override
    public Double getArea() {
        return circle.getArea() * this.height;
    }

    @Override
    public String darw() {
        return "Draw Cylinder";
    }
}
