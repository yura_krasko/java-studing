package org.hillel_Elem.homeworkpatterns.factory;

public  class Circle implements Figure {

    private Double radius = 1.4;

    public Circle() {
    }

    @Override
    public Double getArea() {
        return this.radius + this.radius * Math.PI;
    }

    @Override
    public String darw() {
        return "Draw Circle";
    }
}
