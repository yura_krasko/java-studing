package org.hillel_Elem.homeworkpatterns.builder;

public class GermanCarBuilder implements CarBuilder {

    private Car car;

    public GermanCarBuilder() {

        car = new Car();
    }


    @Override
    public void buildName() {

        car.setName("Aydi");

    }

    @Override
    public void buildColor() {

        car.setColor("green");
    }

    @Override
    public void buildDoors() {

        car.setDoors(2);
    }

    @Override
    public void buildSpeed() {

        car.setSpeed(240);
    }

    @Override
    public Car getCar() {
        return null;
    }
}
