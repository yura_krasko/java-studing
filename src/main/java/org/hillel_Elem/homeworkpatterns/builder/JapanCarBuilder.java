package org.hillel_Elem.homeworkpatterns.builder;

public class JapanCarBuilder implements CarBuilder {

    private Car car;

    public JapanCarBuilder() {

        car = new Car();
    }


    @Override
    public void buildName() {

        car.setName("Toyota");
    }

    @Override
    public void buildColor() {

        car.setColor("red");
    }

    @Override
    public void buildDoors() {

        car.setDoors(4);
    }

    @Override
    public void buildSpeed() {

        car.setSpeed(120);
    }

    @Override
    public Car getCar() {
        return null;
    }
}
