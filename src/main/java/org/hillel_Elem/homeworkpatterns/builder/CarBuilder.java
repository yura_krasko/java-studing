package org.hillel_Elem.homeworkpatterns.builder;

public interface CarBuilder {

    void buildName();
    void buildColor();
    void buildDoors();
    void buildSpeed();
    Car getCar();
}
