package org.hillel_Elem.homeworkpatterns.builder;

public class CarDirector {

    private CarBuilder carBuilder = null;

    public CarDirector(CarBuilder carBuilder) {

        this.carBuilder = carBuilder;
    }

    public CarDirector constructCar(){

        carBuilder.buildName();
        carBuilder.buildColor();
        carBuilder.buildDoors();
        carBuilder.buildSpeed();
        return this;
    }

    public Car getCar(){

        return carBuilder.getCar();
    }
}
