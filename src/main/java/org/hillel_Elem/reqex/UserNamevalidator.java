package org.hillel_Elem.reqex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserNamevalidator {

    private Pattern pattern;
    private Matcher matcher;

    /**
     * ^ - Start of the line
     * [a-z0-9_-] - MAtch characters and symbols in the list, a-z, 0-9, underscore, hyphen
     * {3,15} - Length at least 3 characters and maximum length of 15
     * $ - End of the line
     */
    private static final String USER_PATTER = "^[a-z0-9_-]{3,15}$";


    public UserNamevalidator(){

        pattern = Pattern.compile(USER_PATTER);
    }

    /**
     * Validate user name with regular expression
     *
     * @param userName  - username validation
     * @return  - true valid userbane, false invalid username
     */
    public boolean validate(String userName){

        matcher = pattern.matcher(userName);
        return matcher.matches();
    }
}
