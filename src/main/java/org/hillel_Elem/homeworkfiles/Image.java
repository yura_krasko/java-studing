package org.hillel_Elem.homeworkfiles;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Image {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        File fileFirst = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/image/1.png");
        File fileSecond = new File ("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/image/2.png");
        File fileThird  =new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/image/3.png");
        File fileFourth = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/image/4.png");

        try {
            BufferedImage firstImage = ImageIO.read(fileFirst);
            BufferedImage secondImage = ImageIO.read(fileSecond);
            BufferedImage thirdImage = ImageIO.read(fileThird);
            BufferedImage fourthImage = ImageIO.read(fileFourth);

            BufferedImage rezFirst = compareImage(secondImage, thirdImage);
            BufferedImage rezSecond = compareImage(rezFirst, fourthImage);

            File rezult = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/image/test1.png");
            ImageIO.write(compareImage(firstImage, rezSecond), "png", rezult);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     *
     * @param firstImage
     * @param secondImage
     * @return
     */
    public static BufferedImage compareImage(BufferedImage firstImage, BufferedImage secondImage){

            for (int y = 0; y < firstImage.getHeight(); y++){
                for (int x = 0; x < firstImage.getWidth(); x++){

                    int clr = firstImage.getRGB(x, y);
                    int lll = secondImage.getRGB(x, y);
                    if (clr != lll){

                        secondImage.setRGB(x, y, Color.RED.getRGB());
                    }
                }
            }

            return  secondImage;
    }
}