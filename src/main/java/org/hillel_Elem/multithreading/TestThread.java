package org.hillel_Elem.multithreading;

public class TestThread {

    public static void main(String[] args) {

        RunnableDemo r = new RunnableDemo("Thread-1");
        r.start();

        RunnableDemo r1 = new RunnableDemo("Thread-2");
        r1.start();
    }
}
