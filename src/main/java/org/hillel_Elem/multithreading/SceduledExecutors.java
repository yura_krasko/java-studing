package org.hillel_Elem.multithreading;

import java.util.concurrent.*;

public class SceduledExecutors {

    public static void main(String[] args) {

        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

        Runnable task = ()-> System.out.println("Scheduling: " + System.currentTimeMillis());
        ScheduledFuture<?> future = executor.schedule(task, 3, TimeUnit.SECONDS);


            System.out.println(System.currentTimeMillis());
            //TimeUnit.MILLISECONDS.sleep(1337);
            long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
            System.out.printf("Remaining Delay: %s", remainingDelay);
            System.out.println();

    }
}
