package org.hillel_Elem.multithreading;

import java.util.concurrent.atomic.AtomicInteger;

public class RunnableDemo implements Runnable {

    private Thread t;
    private String threadName;
    static Integer x = 0;
    static AtomicInteger y = new AtomicInteger(10);


    public RunnableDemo(String name) {
        threadName = name;
        System.out.println("Creating " + threadName);
    }


    @Override
    public void run() {
        System.out.println("Running " + threadName);
        try{
            for (int i = 4; i > 0 ; i--) {
                System.out.println("Thread: " + threadName + ", " + i);
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread " + threadName + " enterrupted");
        }

        System.out.println("Thread " + threadName + " exiting");
    }


    public static void add(){

        synchronized (x){
            ++x;
            System.out.println(x);
        }

        y.incrementAndGet();
        System.out.println(y);
    }


    public void start(){
        System.out.println("Starting " + threadName);

        if(t==null){
            t = new Thread(this, threadName);
            t.start();
        }
    }
}
