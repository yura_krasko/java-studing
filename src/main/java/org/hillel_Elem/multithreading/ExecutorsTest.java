package org.hillel_Elem.multithreading;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorsTest {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() ->{
            String threadName = Thread.currentThread().getName();
            try{
                TimeUnit.SECONDS.sleep(3);
            }catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Hello " + threadName);
        });

        try{
            System.out.println("attempt to shutdown executor");
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            if (!executor.isTerminated()){
                System.err.println("Cancel non-finished tasks");
            }
            executor.shutdownNow();
            System.out.println("shutdown finish");
        }

    }
}
