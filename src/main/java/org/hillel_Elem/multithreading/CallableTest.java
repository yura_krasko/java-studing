package org.hillel_Elem.multithreading;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;



public class CallableTest {

    public static void main(String[] args) {

        Callable<Integer> task = () -> {
            try {
                TimeUnit.SECONDS.sleep(1);
                return 123;
            } catch (InterruptedException e) {
                throw new IllegalArgumentException("task interrupted ", e);
            }
        };

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Integer> future = executor.submit(task);


        System.out.println("future done? " + future.isDone());

        Integer rezult = null;
        try {
            rezult = future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            executor.shutdownNow();
        }

        System.out.println("future done? " + future.isDone());
        System.out.println("rezult " + rezult);

        invokeAll();

        ExecutorService excuter2 = Executors.newWorkStealingPool();

        List<Callable<String>> callables = Arrays.asList(
                callable("task1", 2),
                callable("task2", 1),
                callable("task3", 3));

        String rezult2 = null;
        try {
            rezult2 = excuter2.invokeAny(callables);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }finally {
            excuter2.shutdownNow();
        }

        System.out.println(rezult2);
    }




        public static void invokeAll(){

            ExecutorService executor = Executors.newSingleThreadExecutor();

            List<Callable<String>> callables = Arrays.asList(
                    () -> "task1",
                    () -> "task2",
                    () -> "task3");

            try{

                executor.invokeAll(callables)
                        .stream()
                        .map(future -> {
                            try {
                                return  future.get();
                            }catch (Exception e){
                                throw new IllegalArgumentException(e);
                            }
                        })
                        .forEach(x -> {
                            try {
                                TimeUnit.SECONDS.sleep(2);
                                System.out.println(x);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        });
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                executor.shutdownNow();
            }

    }


    static  Callable<String> callable(String rezult, long sleepSecond){
            return  ()->{
                TimeUnit.SECONDS.sleep(sleepSecond);
                return rezult;
            };
    }
}

