package org.hillel_Elem.junit;

import java.util.StringTokenizer;

public class DataMethods {

    /**
     *
     * @param x
     * @param y
     * @return
     */
    public static int sum(int x, int y){

        return x + y;
    }

    /**
     *
     * @param x
     * @param y
     * @return
     */
    public int multiply(int x, int y){

        return x * y;
    }

    /**
     *
     * @param arr
     * @return
     */
    public static int findMax(int arr[]){

        int max = arr[0];
        for (int i = 1; i < arr.length; i++){
            if(max < arr[i]){

                max = arr[i];
            }
        }

        return max;
    }

    /**
     *
     * @param n
     * @return
     */
    public  static int cube(int n){

        return n * n * n;
    }

    /**
     *
     * @param str
     * @return
     */
    public static String reverseWork(String str){

        StringBuilder rezult  = new StringBuilder();
        StringTokenizer tokenizer = new StringTokenizer(str, " ");

        while (tokenizer.hasMoreElements()){
            StringBuilder sb = new StringBuilder();
            sb.append(tokenizer.nextToken());
            sb.reverse();

            rezult.append(sb);
            rezult.append(" ");
        }

        return rezult.toString();
    }


}
