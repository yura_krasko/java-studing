package org.hillel_Elem.exeptions;

public class CustomerService {

    public Customer findByName(String name) throws NameNotFoundExeption{

        if("".equals(name)){

            throw new NameNotFoundExeption(666,"Name is empty!" );
        }

        return new Customer(name);
    }
}
