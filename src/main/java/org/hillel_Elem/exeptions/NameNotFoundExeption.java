package org.hillel_Elem.exeptions;

public class NameNotFoundExeption extends Exception{

    private int errCode;


    public NameNotFoundExeption(int errCode, String message) {
        super(message);
        this.errCode = errCode;
    }



    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }
}
