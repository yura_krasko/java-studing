package org.hillel_Elem.exeptions;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class AppZip {

    List<String> fileList;
    private static final String OUTPUT_ZIP_FILE = "/home/yura/IdeaProjects/hillel_Elem/src/main/resources/MyZIP.zip";
    private static final String SOURCE_FOULDER = "/home/yura/IdeaProjects/hillel_Elem/src/main/resources";

    public AppZip() {


        fileList = new ArrayList<>();
    }

    public static void main(String[] args) {

        AppZip appZip = new AppZip();
        appZip.generateFileList(new File(SOURCE_FOULDER));
        appZip.zipIt(OUTPUT_ZIP_FILE);

    }

    /**
     *
     * @param zipFile
     */
    public void zipIt(String zipFile){

        byte[] buffer = new byte[1024];

        try (

            FileOutputStream fos  = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos)) {

            System.out.println("Output to Zip: " + zipFile);

            for (String file : this.fileList) {


                ZipEntry ze = new ZipEntry(file);
                zos.putNextEntry(ze);

                try (FileInputStream in = new FileInputStream(SOURCE_FOULDER + File.separator + file)) {

                    int len;
                    while ((len = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                    System.out.println("File Added: " + file);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            fos.flush();
            zos.closeEntry();

            System.out.println("Done");
        } catch (IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * traverse a directory an get all files
     * and add the file into fileList
     *
     * @param node
     */
    public void generateFileList(File node){

        if(node.isFile()){
            fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
        }

        if(node.isDirectory()){

            String[] subNote = node.list();
            for(String filename : subNote){
                generateFileList(new File(node, filename));
            }
        }
    }

    /**
     *Fromat the file path for zip
     * @param file file Path
     * @return Fromatted file path
     */
    private String generateZipEntry(String file) {

        return file.substring(SOURCE_FOULDER.length()+1, file.length());

    }
}
