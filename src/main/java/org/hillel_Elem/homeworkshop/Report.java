package org.hillel_Elem.homeworkshop;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.HorizontalListBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.subtotal.AggregationSubtotalBuilder;
import net.sf.dynamicreports.report.constant.Calculation;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.hillel_Elem.homeworkshop.model.AcountData;
import org.hillel_Elem.homeworkshop.model.Customer;
import org.hillel_Elem.homeworkshop.model.Invoice;

import java.math.BigDecimal;


import static net.sf.dynamicreports.report.builder.DynamicReports.*;

public class Report {

Invoice invoice = new Invoice();


    public Report(Invoice data) {
        this.invoice = data;
    }

    /**
     * Create invoice
     *
     * @return
     * @throws DRException
     */
    public JasperReportBuilder builder() throws DRException {

        JasperReportBuilder report = report();
        TextColumnBuilder<String> columnIteam = col.column("ITEM DESCRIPTION", "name", type.stringType());
        TextColumnBuilder<Integer> columnCount = col.column("QUANTITY", "quantity", type.integerType());
        TextColumnBuilder<BigDecimal> columnPrice = col.column("UNIT. PRICE", "price", type.bigDecimalType());

        TextColumnBuilder<BigDecimal> totalColumn = columnPrice.multiply(columnCount).setTitle("Total")
                .setDataType(Templates.currencyType);

        AggregationSubtotalBuilder<BigDecimal> subTotal = sbt.sum(totalColumn);

        TextColumnBuilder<BigDecimal> vatSum = totalColumn.multiply(0.1)
                .setDataType(Templates.currencyType);

        TextColumnBuilder<BigDecimal> discount = totalColumn.multiply(0.05)
                .setDataType(Templates.currencyType);

        TextColumnBuilder<BigDecimal> grandTotal = totalColumn.subtract(vatSum).subtract(discount)
                .setDataType(Templates.currencyType);

        report
                .title(
                    cmp.horizontalList()
                        .add(cmp.image("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/shop/image/header4.png"))
                        .newRow(),
                        cmp.horizontalList().setStyle(stl.style(10)).setGap(10).add(
                                cmp.hListCell(createCustomerComponent("INVOICE TO", invoice.getCustomer())
                                        .setStyle(Templates.invoiceComponent)),
                                cmp.hListCell(createAcountDataComponent("INVOICE", invoice.getAcount())
                                        .setStyle(Templates.invoiceComponent))
                        )
                )
                .setColumnTitleStyle(Templates.columnTitleStyle)
                .columns(columnIteam, columnPrice.setDataType(Templates.currencyType),  columnCount, totalColumn).setColumnStyle(Templates.columnStyle)
                .columnGrid(
                        columnIteam, columnPrice,columnCount,
                        grid.horizontalColumnGridList()
                            .newRow()
                                .add(totalColumn)
                             .newRow()
                            .add(vatSum)
                            .newRow()
                            .add(discount)
                            .newRow()
                            .add(grandTotal)
                )
                .subtotalsAtSummary(
                        sbt.aggregate(exp.text("Total sum"), columnCount, Calculation.NOTHING)
                                .setStyle(Templates.atSumarryStyle),
                        sbt.sum(totalColumn).setStyle(Templates.atSumarryStyle),
                        sbt.aggregate(exp.text("Tax: VAT 10%"), columnCount, Calculation.NOTHING)
                                .setStyle(Templates.atSumarryStyle),
                        sbt.sum(vatSum).setStyle(Templates.atSumarryStyle),
                        sbt.aggregate(exp.text("Discount"), columnCount, Calculation.NOTHING)
                                .setStyle(Templates.atSumarryStyle),
                        sbt.sum(discount).setStyle(Templates.atSumarryStyle),
                        sbt.aggregate(exp.text("GRAND TOTAL"), columnCount, Calculation.NOTHING)
                                .setStyle(Templates.grandTotalStyle),
                        sbt.sum(grandTotal).setStyle(Templates.grandTotalStyle)
                )
                .summary(
                                cmp.horizontalList().setStyle(stl.style(20)).setGap(10).add(
                                cmp.hListCell(createPaymentDataComponent("PAYMENT METHODS WE ACCEPT")),
                                cmp.hListCell(cmp.text("Accounts Manager").setStyle(Templates.accontsStyle)
                                )
                        ),
                        cmp.text("Daniel THOMSON").setStyle(Templates.managerStyle),
                        cmp.horizontalList().setStyle(stl.style(10)).setGap(10).add(

                                cmp.hListCell(cmp.text("Payment terms: 30 days").setRows(2)
                                        .setStyle(Templates.textTermStyle)),
                                cmp.hListCell(cmp.image("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/1437192909_vygoda-ot-pokupki-faksimile.jpg")
                                        .setFixedDimension(150, 80).setStyle(Templates.imageStyle)))
                )
                .pageFooter(
                        cmp.text("Thanks you for your business!").setStyle(Templates.footerTextStyle),
                        cmp.horizontalList().setStyle(stl.style(10)).setGap(10).add(
                                cmp.image("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/shop/image/photo_2018-01-25_18-06-28.jpg")
                                        .setFixedDimension(20, 20),
                                cmp.text("01 222 777345").setStyle(Templates.footerStyle),
                                cmp.image("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/shop/image/photo_2018-01-25_16-26-41.jpg")
                                        .setFixedDimension(20, 20),
                                cmp.text("test@gmail.com").setStyle(Templates.footerStyle),
                                cmp.image("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/shop/image/photo_2018-01-25_18-06-14.jpg")
                                        .setFixedDimension(20, 20),
                                cmp.text("test.com.ua").setStyle(Templates.footerStyle),
                                cmp.image("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/shop/image/address.png")
                                        .setFixedDimension(20, 20),
                                cmp.text("Washigton Street 45/2").setStyle(Templates.footerStyle)),
                        cmp.horizontalList().setStyle(Templates.footerStyle)
                )
                .setDataSource(createDataSource(invoice));

        return report;

    }


    /**
     * Vertival list information by customer
     * @param label
     * @param customer
     * @return
     */
    private VerticalListBuilder createCustomerComponent(String label, Customer customer){

        HorizontalListBuilder list = cmp.horizontalList();
        addCustomerAttribute(list, "Name", customer.getName());
        addCustomerAttribute(list, "City", customer.getCity());
        addCustomerAttribute(list, "Address", customer.getAddress());
        addCustomerAttribute(list, "Telephone", customer.getTelephone());
        addCustomerAttribute(list, "Email", customer.getEmail());
        return cmp.verticalList(
                cmp.text(label).setStyle(Templates.attributeStyle),
                    list);

    }


    /**
     *
     * @param list
     * @param label
     * @param value
     */
    private void  addCustomerAttribute(HorizontalListBuilder list, String label, String value){

        if(value != null){
            list.add(cmp.text(label + ":").setStyle(Templates.labelStyle),
                    cmp.text(value).setStyle(Templates.valueStyle))
                    .newRow();
        }
    }


    /**
     *
     * @param list
     * @param label
     * @param value
     */
    private void  addAcountDataAttribute(HorizontalListBuilder list, String label, String value){

        if(value != null){
            list.add(cmp.text(label + ":").setStyle(Templates.labelStyle),
                    cmp.text(value).setStyle(Templates.valueStyle))
                    .newRow();
        }
    }


    /**
     * Information by oredr
     *
     * @param label
     * @param acount
     * @return
     */
    private VerticalListBuilder createAcountDataComponent(String label, AcountData acount){

        HorizontalListBuilder list = cmp.horizontalList();

        addAcountDataAttribute(list, "Invoice #", acount.getInvoiceNumber().toString());
        addAcountDataAttribute(list, "Acount #", acount.getAccount().toString());
        addAcountDataAttribute(list, "Invoice Date", acount.getCurrentDate().toString());

            return cmp.verticalList(
                    cmp.text(label).setStyle(Templates.attributeStyle),
                    list);
    }


    /**
     *
     * @param list
     * @param label
     */
    private void  paymentAttribute(HorizontalListBuilder list, String label){

            list.add(cmp.text(label).setStyle(Templates.paymentStyle))
                    .newRow();

    }


    /**
     * Payment method information
     *
     * @param label
     * @return
     */
    private VerticalListBuilder createPaymentDataComponent(String label){

        HorizontalListBuilder list = cmp.horizontalList();

        paymentAttribute(list, "Paylol: bills.paypol@company.com");
        paymentAttribute(list, "CardPayment: Visa, Master Card");
        paymentAttribute(list, "We accep Cheque.");

        return cmp.verticalList(
                cmp.text(label).setStyle(Templates.sumaryStyle),
                list);
    }

    /**
     * Get information by oredr
     * @param invoice
     * @return
     */
    public JRDataSource createDataSource(Invoice invoice){

        return new JRBeanCollectionDataSource(invoice.getOrders());
    }

}
