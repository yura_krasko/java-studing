package org.hillel_Elem.homeworkshop;


import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.exception.DRException;
import org.hillel_Elem.homeworkshop.model.AcountData;
import org.hillel_Elem.homeworkshop.model.Customer;
import org.hillel_Elem.homeworkshop.model.Invoice;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Scanner;

public class DataInvoice {

    Orders orders = new Orders();
    Customers customers = new Customers();
    Products products = new Products();
    Scanner scanner = new Scanner(System.in);
    public static int ch = 0;
    Invoice invoice = new Invoice();


    public Invoice invoiceData(){

        invoice.setOrders(orders.orderList);
        invoice.setAcount(createAcount(100001, 1000012345, LocalDate.now()));

        return invoice;
    }

    /**
     * Input number
     *
     * @return
     */
    public int getInput(){
        ch = scanner.nextInt();
        return ch;
    }

    /**
     * Add new customer
     *
     */
    public void addCustomer(){

        System.out.print("Input name: ");
        String name = scanner.nextLine();
        System.out.print("Input city: ");
        String city= scanner.nextLine();
        System.out.print("Input address: ");
        String address = scanner.nextLine();
        System.out.print("Input telephone: ");
        String tel = scanner.nextLine();
        System.out.print("Input email: ");
        String email = scanner.nextLine();

        customers.addCustomer(new Customer(customers.getMaxId().getId() + 1, name, city, address, tel, email));
        customers.writeCustomer();
        System.out.println("Thank you for registration!\n");
    }

    /**
     * Get customer by email
     *
     */
    public void getCustomerByEmail(){
        System.out.print("Input your Email: ");
        Scanner scanner = new Scanner(System.in);
        String email = scanner.nextLine();
        invoice.setCustomer(customers.getCustomerByEmail(email));
        System.out.println("Hello, " + customers.getCustomerName() + "!");
        System.out.println();

    }

    /**
     * Add product to the curt
     *
     */
    public void addProductToCart(){
        System.out.println("Add product to cart");
        System.out.print("Input article product: ");
        Integer article = getInput();
        System.out.print("Input quantity product: ");
        Integer quantity = getInput();
        orders.addProdustToOrder(article, quantity);
        System.out.println("Product add to cart!\n");
    }

    /**
     * Remove product from cart
     *
     */
    public void removeProductFromCart(){
        System.out.print("Input article: ");
        Integer article = getInput();
        orders.removeProdustToOrder(article);
    }

    /**
     *Create account data
     *
     * @param number
     * @param account
     * @param date
     * @return
     */
    public AcountData createAcount(Integer number, Integer account, LocalDate date){

        AcountData acount = new AcountData();
        acount.setInvoiceNumber(number);
        acount.setAccount(account);
        acount.setCurrentDate(date);

        return acount;
    }

    /**
     * Show products in cart
     *
     */
    public void showCart(){
        System.out.println();
        orders.printOrder();
    }

    /**
     * show all products
     *
     */
    public void printProduct(){
        products.printAllProducts();
    }

    /**
     * Write invoice folder
     *
     */
    public void saveInvoice(){

        Report report = new Report(invoice);
        String PROJECT_PATH = "/home/yura/IdeaProjects/hillel_Elem/src/main/resources/shop/Order";
        String nameDirectory = customers.getCustomerId().toString();
        Path path = Paths.get(PROJECT_PATH, nameDirectory);
        try {
            Files.createDirectories(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            JasperReportBuilder reportBuilder = report.builder();

            String pathToFile = path + "/invoice.pdf";
            reportBuilder.toPdf(new FileOutputStream(new File(pathToFile)));
            System.out.println("Thank you for shopping!");

        } catch (DRException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
