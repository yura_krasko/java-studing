package org.hillel_Elem.homeworkshop.model;

import java.math.BigDecimal;

public class Product {

    private Integer article;
    private String name;
    private BigDecimal price;

    public Product(Integer article, String name, BigDecimal price) {
        this.article = article;
        this.name = name;
        this.price = price;
    }

    public Product() {
    }

    public Integer getArticle() {
        return article;
    }

    public void setArticle(Integer article) {
        this.article = article;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
