package org.hillel_Elem.homeworkshop.model;

import java.util.List;

public class Invoice {

    private AcountData acount;
    private Customer customer;
    private List<Order> orders;

    public AcountData getAcount() {
        return acount;
    }

    public void setAcount(AcountData acount) {
        this.acount = acount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
