package org.hillel_Elem.homeworkshop;


import org.hillel_Elem.homeworkshop.model.Order;
import org.hillel_Elem.homeworkshop.model.Product;

import java.util.ArrayList;
import java.util.List;

public class Orders {

    List<Order> orderList = new ArrayList<>();

    Products products = new Products();

    /**
     *Add product to order
     *
     * @param article
     * @param quantity
     */
    public void addProdustToOrder(Integer article, Integer quantity){

        Product product = products.getProductByArticle(article);
        orderList.add(new Order(product.getName(), quantity, product.getPrice()));
    }

    /**
     *Remove product from order
     *
     * @param article
     */
    public void removeProdustToOrder(Integer article){

        Product product = products.getProductByArticle(article);
        orderList.remove(product);
    }

    /**
     *Show all products in order
     *
     */
    public void printOrder(){
        System.out.println("Products in the cart:");
        System.out.printf("|%-8s|%-8s|%n", "Name", "Quantity");
        System.out.println("============================");
        orderList.stream().forEach(x -> {
            System.out.printf("|%-8s", x.getName());
            System.out.printf("|%-8s|%n", x.getQuantity());
        }
        );
    }
}
