package org.hillel_Elem.homeworkshop;

import java.util.Scanner;

public class UI {

    DataInvoice data = new DataInvoice();
    Report report = new Report(data.invoiceData());

    public static int number = 0;
    public static String symbol = null;

    /**
     * Input number
     *
     * @return
     */
    public int getInputInt(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input: ");
        number = scanner.nextInt();
        return number;
    }

    /**
     * Input symbol
     *
     * @return
     */
    public String getInputString(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input: ");
        symbol = scanner.nextLine();
        return symbol;
    }

    /**
     * Main menu
     */
    public void menu(){
        do{

            System.out.println("=============================");
            System.out.println("1. Print products" );
            System.out.println("2. Add product to curt ");
            System.out.println("3. Show cart");
            System.out.println("4. Place your order");
            System.out.println("0. Exit");
            System.out.println("=============================");
            getInputInt();
            switch (number){
                case 1: {
                    data.printProduct();
                    break;
                }
                case 2 : {
                    innerMenu();
                    data.addProductToCart();
                    innerMenu1();
                    break;
                }
                case 3: {
                    data.showCart();
                    innerMenu2();
                    break;
                }
                case 4 : {
                    data.saveInvoice();
                    break;
                }
                case 5 : {
                    data.addCustomer();
                    break;
                }
                case 6: {
                    data.getCustomerByEmail();
                    break;
                }
                case 0 : {
                    System.exit(0);
                }
            }
        } while (number !=0);
    }

    /**
     * Inner menu
     */
    public void innerMenu(){
        System.out.println("=============================");
        System.out.println("Are you registration? y/n");
        System.out.println("=============================");
        getInputString();
         switch (symbol){
                case "y" :
                    data.getCustomerByEmail();
                    break;
                case "n" :
                    System.out.println("Form registration");
                    data.addCustomer();
                    data.getCustomerByEmail();
                    break;
                default:
                    break;
            }
    }

    /**
     * Inner menu
     */
    public void innerMenu1(){
        System.out.println("Add other product to cart? y/n");
        getInputString();
        switch (symbol){
            case "y" :
                data.addProductToCart();
                break;
            case "n" :
                break;
            default:
                break;
        }
    }

    /**
     * Inner menu
     */
    public void innerMenu2(){
        System.out.println("Remove product from cart? y/n");
        getInputString();
        switch (symbol){
            case "y" :
                data.removeProductFromCart();
                break;
            case "n" :
                break;
            default:
                break;
        }
    }
}






