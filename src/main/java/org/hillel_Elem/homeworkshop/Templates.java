package org.hillel_Elem.homeworkshop;

import net.sf.dynamicreports.report.builder.datatype.BigDecimalType;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;

import java.awt.*;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;

public class Templates {

    public static final StyleBuilder boldStyle;
    public static final StyleBuilder boldRightStyle;
    public static final StyleBuilder boldLeftStyle;
    public static final StyleBuilder underLineStyle;
    public static final StyleBuilder boldCenteredStyle;
    public static final StyleBuilder columnTitleStyle;
    public static final StyleBuilder columnStyle;
    public static final CurrencyType currencyType;
    public static final StyleBuilder subtotalStyle;
    public static final StyleBuilder headerStyle;
    public static final StyleBuilder underlineStyle;
    public static final StyleBuilder italicStyle;
    public static final StyleBuilder invoiceComponent;
    public static final StyleBuilder attributeStyle;
    public static final StyleBuilder atSumarryStyle;
    public static final StyleBuilder sumaryStyle;
    public static final StyleBuilder accontsStyle;
    public static final StyleBuilder managerStyle;
    public static final StyleBuilder footerStyle;
    public static final StyleBuilder grandTotalStyle;
    public static final StyleBuilder valueStyle;
    public static final StyleBuilder labelStyle;
    public static final StyleBuilder paymentStyle;
    public static final StyleBuilder imageStyle;
    public static final StyleBuilder textTermStyle;
    public static final StyleBuilder footerTextStyle;

    static {

        boldStyle = stl.style().bold();

        boldRightStyle = stl.style(boldStyle)
                .setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);

        boldLeftStyle = stl.style().boldItalic()
                .setFontSize(10);

        underLineStyle = stl.style()
                .underline()
                .setFontSize(20)
                .setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);

        boldCenteredStyle = stl.style(boldStyle)
                .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                .setFontSize(30)
                .setPadding(25);

        columnTitleStyle = stl.style(boldCenteredStyle)
                .setTopBorder(stl.pen2Point()
                        .setLineColor(Color.black)).setBottomBorder(stl.pen2Point())
                        .setBackgroundColor(new Color(16, 78, 139))
                        .setForegroundColor(Color.white)
                        .setFontSize(10).setPadding(10);

        columnStyle = stl.style(boldCenteredStyle)
                .setBackgroundColor(Color.LIGHT_GRAY)
                .setPadding(10)
                .setFontSize(8)
                .setBottomBorder(stl.pen2Point());

        subtotalStyle = stl.style(boldStyle)
                .setBorder(stl.pen1Point())
                .setBackgroundColor(Color.GRAY)
                .setFontSize(10)
                .setPadding(10);

        currencyType = new CurrencyType();

        headerStyle = stl.style()
                .setBorder(stl.penDotted())
                .setBackgroundColor(Color.LIGHT_GRAY)
                .setPadding(7).setFontSize(13);

        underlineStyle = stl.style().underline().bold();

        italicStyle = stl.style().italic();

        attributeStyle = stl.style().bold()
                .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                .setPadding(7).setFontSize(20)
                .setForegroundColor(new Color(16, 78, 139));

        invoiceComponent = stl.style().boldItalic().setFontSize(20);

        atSumarryStyle = stl.style().boldItalic().setPadding(7)
                .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                .setTopBorder(stl.pen2Point()
                        .setLineColor(Color.black))
                .setBottomBorder(stl.pen2Point())
                .setBackgroundColor(new Color(16, 78, 139))
                .setBottomBorder(stl.pen2Point())
                .setForegroundColor(Color.LIGHT_GRAY)
                .setFontSize(10);

        accontsStyle = stl.style().italic().setForegroundColor(Color.GRAY)
                .setRightIndent(60)
                .setTopPadding(65)
                .setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);

        managerStyle = stl.style().boldItalic().setFontSize(15).setRightIndent(50)
                .setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);

        footerStyle = stl.style().boldItalic()
                .setFontSize(8)
                .setTopPadding(5)
                .setForegroundColor(Color.white)
                .boldItalic().setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                .setBackgroundColor(new Color(16, 78, 139));

        grandTotalStyle = stl.style(boldStyle)
                    .setBackgroundColor(new Color(16, 78, 139))
                    .setFontSize(13).setPadding(10)
                    .setForegroundColor(Color.white)
                    .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                    .setTopBorder(stl.pen2Point()
                            .setLineColor(Color.black))
                    .setBottomBorder(stl.pen2Point());

        valueStyle = stl.style().italic()
                .setForegroundColor(Color.DARK_GRAY)
                .setPadding(2)
                .setLeftPadding(15)
                .setFontSize(12);

        labelStyle = stl.style().boldItalic()
                .setForegroundColor(Color.DARK_GRAY)
                .setPadding(2)
                .setLeftPadding(40)
                .setFontSize(12);

        sumaryStyle = stl.style().boldItalic()
                .setFontSize(12)
                .setTopPadding(10)
                .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                .setBackgroundColor(new Color(16, 78, 139))
                .setForegroundColor(Color.white);

        paymentStyle = stl.style().italic()
                .setForegroundColor(Color.white)
                .setLeftIndent(20)
                .setBackgroundColor(new Color(16, 78, 139))
                .setPadding(2)
                .setFontSize(8);

        imageStyle = stl.style().setLeftIndent(800);

        textTermStyle = stl.style().boldItalic()
                .setFontSize(20).setTopPadding(20)
                .setForegroundColor(new Color(16, 78, 139));

        footerTextStyle = stl.style().setFontSize(15)
                .setForegroundColor(new Color(16, 78, 139))
                .boldItalic().setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);

    }


    public static class CurrencyType extends BigDecimalType {

        @Override
        public String getPattern() {
            return "$ #,###.00";
        }
    }
}
