package org.hillel_Elem.homeworkshop;


import org.hillel_Elem.homeworkshop.model.Product;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Products {

    static final String SOURCE_PRODUCT = "/home/yura/IdeaProjects/tasks/src/main/resources/shop/PriceList.csv";
    private Product product = null;

    public Products() {
    }

    /**
     *Show all products
     */
    public void printAllProducts(){

        try(Stream<String> stream = Files.lines(Paths.get(SOURCE_PRODUCT))) {

            List<Product> products = new ArrayList<>();
            products = stream.map(line -> {
                    String [] s = line.split(",");
                    return new Product(
                            Integer.parseInt(s[0]),
                            s[1],
                            new BigDecimal(s[2]));
                    })
                    .collect(Collectors.toList());

            System.out.printf("|%-8s|%-8s|%-8s|%n", "Article", "Name", "Price");
            System.out.println("============================");

            products.stream().forEach(x -> {
                System.out.printf("|%-8s", x.getArticle());
                System.out.printf("|%-8s", x.getName());
                System.out.printf("|%-8s|%n", x.getPrice());});

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Get product by article
     *
     * @param article
     * @return
     */
    public Product getProductByArticle(Integer article){

        try(Stream<String> stream = Files.lines(Paths.get(SOURCE_PRODUCT))) {

            List<Product> products = new ArrayList<>();
            products = stream.map(line -> {
                            String [] s = line.split(",");
                            return new Product(
                                Integer.parseInt(s[0]),
                                s[1],
                                new BigDecimal(s[2]));
                        }).collect(Collectors.toList());

            for (Product prod : products){
                if (article.equals(prod.getArticle())){
                    product = prod;
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return product;
    }
}
