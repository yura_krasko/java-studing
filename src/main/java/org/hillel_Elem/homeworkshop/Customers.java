package org.hillel_Elem.homeworkshop;



import org.hillel_Elem.homeworkshop.model.Customer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Customers {

    List<Customer> customerList = new ArrayList<>();
    Customer customer;

    static final String SOURCE_CUSTOMER = "/home/yura/IdeaProjects/tasks/src/main/resources/shop/Customers.csv";
    static final String DELIMETER = ",";

    /**
     * Add new customer
     *
     * @param customer
     */
    public void addCustomer(Customer customer) {

        customerList.add(customer);
    }

    /**
     *Get customer by email
     *
     * @param email
     * @return
     */
    public Customer getCustomerByEmail(String email){

        List<Customer> listCustomers = new ArrayList<>();
        try(Stream<String> stream = Files.lines(Paths.get(SOURCE_CUSTOMER))) {

            listCustomers = stream.map(line ->{
                        String [] s = line.split(",");
                        return new Customer(
                                Integer.parseInt(s[0]),
                                s[1],
                                s[2],
                                s[3],
                                s[4],
                                s[5]);})
                    .collect(Collectors.toList());

            for (Customer cus : listCustomers){
                if (email.equals(cus.getEmail())){
                    customer = cus;
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return customer;
    }

    /**
     * Get customer name
     *
     * @return
     */
    public String getCustomerName(){
        return customer.getName();
    }

    /**
     * Get customer id
     *
     * @return
     */
    public Integer getCustomerId(){
        return customer.getId();
    }

    /**
     *Get max id from customer
     *
     * @return
     */
    public Customer getMaxId(){

        Customer maxId = null;
        List<Customer> listCustomers = new ArrayList<>();
        try(Stream<String> stream = Files.lines(Paths.get(SOURCE_CUSTOMER))) {
            listCustomers = stream.map(line ->{
                String [] s = line.split(",");
                return new Customer(
                        Integer.parseInt(s[0]),
                        s[1],
                        s[2],
                        s[3],
                        s[4],
                        s[5]);})
                    .collect(Collectors.toList());

           maxId = listCustomers.stream().max((x1, x2) -> Integer.compare(x1.getId(), x2.getId().intValue())).get();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return maxId;
    }

    /**
     * Write customer in the file
     */
    public void writeCustomer(){

        StringBuilder builder = new StringBuilder();
        System.out.println();
        for (Customer cust : customerList){
            builder.append(cust.getId());
            builder.append(DELIMETER);
            builder.append(cust.getName());
            builder.append(DELIMETER);
            builder.append(cust.getCity());
            builder.append(DELIMETER);
            builder.append(cust.getAddress());
            builder.append(DELIMETER);
            builder.append(cust.getTelephone());
            builder.append(DELIMETER);
            builder.append(cust.getEmail() + "\n");
        }
        try {
            Files.write(Paths.get(SOURCE_CUSTOMER), builder.toString().getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
