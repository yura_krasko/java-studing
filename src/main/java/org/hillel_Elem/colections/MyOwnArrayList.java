package org.hillel_Elem.colections;

import java.io.Serializable;
import java.util.RandomAccess;

public class MyOwnArrayList <T> implements RandomAccess, Cloneable, Serializable {

    private transient T[] elementData;
    private int size;
    protected transient  int modCount = 0;
    private static final long serialVersionUID = 1234L;

    /**
     *
     */
    public MyOwnArrayList(){

         this (10);
    }

    /**
     *
     * @param inintialCapacity
     */
    public MyOwnArrayList(int inintialCapacity){

         super();
         if (inintialCapacity < 0){
             throw new IllegalArgumentException("Illegal capacity: " + inintialCapacity);
         }

         T t[] = (T[]) new Object[inintialCapacity];
         this.elementData = t;
    }

    /**
     *
     * @param obj
     * @return
     */
    public boolean add(T obj){

        validateCapacity(size + 1);
        elementData[size++]  = obj;
        return  true;
    }

    /**
     *
     * @param index
     * @return
     */
    public T get(int index){

        T obj = elementData[index];
        return obj;
    }

    /**
     *
     * @return
     */
    public int size (){

        return size;
    }

    /**
     *
     * @param minCapacity
     */
    public void validateCapacity(int minCapacity){

        modCount++;
        int oldCapacity = elementData.length;
        if (minCapacity > oldCapacity){
            Object oldData[] = elementData;
            int newCapacity = (oldCapacity * 3)/2;
            if(newCapacity < minCapacity){
                newCapacity = minCapacity;
            }

            T t[] = (T[]) new Object[newCapacity];
            elementData = t;
            System.arraycopy(oldData, 0, elementData, 0, size);
        }
    }

    /**
     *
     * @param index
     * @return
     */
    public T remove(int index){

        if(index >= size){
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }

        modCount++;
        T oldvalue = elementData[index];
        int numMoved = size - index - 1;
        if(numMoved > 0){
            System.arraycopy(elementData, index +1, elementData, index, numMoved);
        }

        elementData[--size] = null;
        return oldvalue;
    }


    /**
     *
     * @return
     */
    public FruitIterator iterator(){

        System.out.println("My overridded method called in Fruit class");
        return new FruitIterator(this);
    }
}
