package org.hillel_Elem.strings;

import java.util.StringTokenizer;

public class StringTokenizerUtil {

    /**
     *
     * @param str
     */

    public static void splitBySpace(String str) {

        StringTokenizer st = new StringTokenizer(str);

        System.out.println("---- Split by space -----");
        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }

    /**
     *
     * @param str
     */
    public static void splitByComma(String str){

        System.out.println("---- Split by comma ',' ----");
        StringTokenizer st2 = new StringTokenizer(str, ",");

        while(st2.hasMoreElements()){
            System.out.println(st2.nextElement());
        }
    }
    }

