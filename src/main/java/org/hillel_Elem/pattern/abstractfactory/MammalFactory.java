package org.hillel_Elem.pattern.abstractfactory;

import org.hillel_Elem.pattern.factory.Animal;
import org.hillel_Elem.pattern.factory.Cat;
import org.hillel_Elem.pattern.factory.Dog;

public class MammalFactory extends  SpeciesFactory {
    @Override
    public Animal getAnimal(String type) {
        if("dog".equals(type)){
            return new Dog();
        } else{
            return new Cat();
        }
    }
}
