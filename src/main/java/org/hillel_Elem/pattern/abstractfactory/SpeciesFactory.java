package org.hillel_Elem.pattern.abstractfactory;

import org.hillel_Elem.pattern.factory.Animal;

public abstract class SpeciesFactory {

    public abstract Animal getAnimal(String type);
}
