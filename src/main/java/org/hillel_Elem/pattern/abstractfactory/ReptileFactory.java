package org.hillel_Elem.pattern.abstractfactory;

import org.hillel_Elem.pattern.factory.Animal;
import org.hillel_Elem.pattern.factory.Snake;
import org.hillel_Elem.pattern.factory.Tyrannosaurus;

public class ReptileFactory extends SpeciesFactory {
    @Override
    public Animal getAnimal(String type) {
        if("snake".equals(type)){
            return new Snake();
        } else{
            return new Tyrannosaurus();
        }

    }
}
