package org.hillel_Elem.pattern.prototype;

public class Dog implements Prototype {

    String sound;

    public Dog(String sound) {
        this.sound = sound;
    }

    /**
     *
     * @return
     */
    @Override
    public Prototype doClone() {
        return new Dog(sound);
    }

    /**
     *
     * @return
     */
    public String toString() {
        return "This dog say " + sound;
    }
}
