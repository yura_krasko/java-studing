package org.hillel_Elem.pattern.prototype;

public interface Prototype {

    Prototype doClone();
}
