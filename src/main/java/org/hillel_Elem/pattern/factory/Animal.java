package org.hillel_Elem.pattern.factory;

public abstract class Animal {

    public abstract String makeSound();
}
