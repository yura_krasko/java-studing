package org.hillel_Elem.pattern.factory;

public class Tyrannosaurus extends Animal {
    @Override
    public String makeSound() {
        return "Roar";
    }
}
