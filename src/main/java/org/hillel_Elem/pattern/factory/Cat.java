package org.hillel_Elem.pattern.factory;

public class Cat extends Animal {
    @Override
    public String makeSound() {
        return "Meau";
    }
}
