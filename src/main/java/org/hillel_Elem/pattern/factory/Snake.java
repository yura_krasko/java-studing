package org.hillel_Elem.pattern.factory;

public class Snake extends Animal {
    @Override
    public String makeSound() {
        return "Hiss";
    }
}
