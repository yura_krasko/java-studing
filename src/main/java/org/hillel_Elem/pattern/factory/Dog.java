package org.hillel_Elem.pattern.factory;

public class Dog extends Animal {
    @Override
    public String makeSound() {
        return "Wolf";
    }
}
