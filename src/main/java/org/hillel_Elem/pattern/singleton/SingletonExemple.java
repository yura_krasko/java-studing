package org.hillel_Elem.pattern.singleton;

public class SingletonExemple {

    private static  SingletonExemple singletonExemple = null;

    private SingletonExemple(){

    }

    /**
     *
     * @return
     */
    public static SingletonExemple getInstance(){

        if(singletonExemple == null){
            singletonExemple = new SingletonExemple();
        }

        return singletonExemple;
    }

    /**
     *
     */
    public void  sayHello(){

        System.out.println("Hello!");
    }
}
