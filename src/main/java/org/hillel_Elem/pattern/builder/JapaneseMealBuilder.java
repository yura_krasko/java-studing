package org.hillel_Elem.pattern.builder;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;

public class JapaneseMealBuilder implements MealBuilder {

    private  Meal meal;

    public JapaneseMealBuilder(){
        meal = new Meal();
    }
    @Override
    public void buildDrink() {
        meal.setDrink("sake");
    }

    @Override
    public void buildMainCourse() {
        meal.setMainCourse("chiken tititaki");
    }

    @Override
    public void buildSide() {
        meal.setSide("miso sup");
    }

    @Override
    public Meal getMeal() {
        return meal;
    }
}
