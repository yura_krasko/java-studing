package org.hillel_Elem.pattern.builder;

public class MealDirector {

    private MealBuilder mealBuilder = null;

    public MealDirector(MealBuilder mealBuilder) {
        this.mealBuilder = mealBuilder;
    }

    public MealDirector constructMeal(){

        mealBuilder.buildDrink();
        mealBuilder.buildMainCourse();
        mealBuilder.buildSide();
        return this;
    }

    public  Meal getMeal(){

        return mealBuilder.getMeal();
    }
}
