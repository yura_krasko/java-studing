package org.hillel_Elem.pattern.builder;

public interface MealBuilder {

    void buildDrink();
    void buildMainCourse();
    void buildSide();
    Meal getMeal();

}
