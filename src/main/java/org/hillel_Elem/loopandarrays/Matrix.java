package org.hillel_Elem.loopandarrays;

public class Matrix {

    public static  Double [][] multiplicar(Double [][] first, Double[][] second){

        int firstRows = first.length;
        int firstColumns = first[0].length;
        int secondRows = second.length;
        int secondColumns = second[0].length;


        if(firstColumns != secondColumns) {
            throw new IllegalArgumentException("First Rows: " + firstColumns + " did not match Second Columns " + secondColumns + ".");
        }

        Double [][] rezult = new Double [firstRows][secondColumns];
        for (int i = 0; i < first.length; i++){
            for (int j = 0; j < second.length; j++){
                rezult [i][j] = 0.00000;
            }
        }

        for (int i = 0; i < firstRows; i++){
            for (int j = 0; j< secondColumns; j++){
                for (int k =0; k < rezult.length; k++){
                    rezult[i][j] += first[i][j] * second[k][j];
                }
            }
        }

        return rezult;
    }
}
