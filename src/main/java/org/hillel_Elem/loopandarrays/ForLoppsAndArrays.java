package org.hillel_Elem.loopandarrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ForLoppsAndArrays {

    /**
     *
     * @param myArray1
     * @param myArray2
     * @return
     */

    public  static  boolean equalityOfTwoArrays(int[] myArray1, int[] myArray2){

        boolean equalOrNot = true;

        if (myArray1.length == myArray2.length){

            for (int i = 0; i < myArray1.length; i++){

                if (myArray1[i] != myArray2[i]){

                    equalOrNot = false;
                }
            }
        } else
            equalOrNot = false;


        return equalOrNot;
    }


    /**
     *
     * @param myArray
     */
    public  static  void uniqueArray (int [] myArray){

        int arraySize = myArray.length;

        System.out.println("Original array: ");

        for (int i = 0; i < arraySize; i++){

            System.out.println(myArray[i] + "\t");
        }
        //Comparing eavh element with all other element
        for (int i = 0; i < arraySize; i++ ){

            for (int j = i +1; j < arraySize; j++){
             //if any two elements with last unique element
                if (myArray[i] == myArray[j]){
                    //Replace duplicate element with  last unique eltmtnr
                    myArray[j] = myArray[arraySize - 1];

                    arraySize--;
                    j--;
                }

            }
        }

        //Copping only unique elements of my_array into array1
        int[] array1 = Arrays.copyOf(myArray, arraySize);
        //Printing arrawithothDublicate
        System.out.println();

        System.out.println("Array with unique values: ");

        for (int i =0; i < array1.length; i++){
            System.out.println(array1[i] + "\t");
        }
    }
}
