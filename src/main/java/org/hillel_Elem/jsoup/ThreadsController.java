package org.hillel_Elem.jsoup;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;


public class ThreadsController implements Runnable {

    private static final Logger logger = Logger.getLogger(ThreadsController.class.getName());

    private String url;
    private int counter;
    private Path path = Paths.get(".").toAbsolutePath().getParent();
    private static boolean DEBAG = false;

    public ThreadsController(String url) {
        this.url = url;

    }

    @Override
    public void run() {

        logger.info("Run " + Thread.currentThread().getName());
        try{
            logger.warn("File will not wrote if path null!");
            File linksFile = new File(path + "/src/main/resources/links", Thread.currentThread().getName() + ".html");
            try(BufferedWriter linksFileWriter = new BufferedWriter(new FileWriter(linksFile, true))){

                Connection.Response html = Jsoup.connect(url).execute();

                linksFileWriter.write(html.body());

                if (DEBAG);
                System.out.println(Thread.currentThread().getName() + "Started");

                counter = TestMain.atomicInteger.incrementAndGet();
                System.out.println(counter + " number of Sites are checked!");
            }
        }catch (IOException e) {
            logger.error("File does not write! ", e);
        }

        logger.info("Finish " + Thread.currentThread().getName());
    }
}
