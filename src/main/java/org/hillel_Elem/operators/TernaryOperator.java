package org.hillel_Elem.operators;

public class TernaryOperator {

    private Integer first;

    private Integer second;

    public Integer getFirst(){
        return  first;
    }

    public Integer getSecond(){
        return  second;
    }

    public TernaryOperator(){

    }

    public TernaryOperator(Integer first, Integer second){
        this.first = first;
        this.second = second;
    }


    /**
     *
     * @param i
     * @param j
     * @return
     */
    public static  int getMinValue(int i, int j){
        return (i < j) ? i : j;
    }

    /**
     *
     * @param i
     * @return
     */
    public static int getAbsoluteValue (int i){
        return (i < 0) ? -1 : i;
    }

    /**
     *
     * @param b
     * @return
     */
    public  static  boolean invertBoolean (boolean b){
        return b ? false : true;
    }

    /**
     *
     * @param str
     */
    public  static  void containsA (String str){
        String data = str.contains("A") ? "Str contains 'A'" : "Str doesn't vontains 'A'";
        System.out.println(data);
    }

    /**
     *
     * @param i
     * @param ternaryOperator
     */
    public  static  void ternaryMethod(Integer i, TernaryOperator ternaryOperator){

        System.out.println((i.equals(ternaryOperator.getFirst())
                ? "i = 5" : ((i.equals(ternaryOperator.getSecond()))) ? "i = 10 " : "i is not tqual to 5 or 10"));
    }
}
