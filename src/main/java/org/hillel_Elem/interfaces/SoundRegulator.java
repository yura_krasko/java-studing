package org.hillel_Elem.interfaces;

public interface SoundRegulator {

    void up();
    void down();
}
