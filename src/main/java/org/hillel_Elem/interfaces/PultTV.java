package org.hillel_Elem.interfaces;

public class PultTV extends Pult implements ButtonSwitch, SoundRegulator {
    /**
     *
     */
    @Override
    public void switchOn() {

        System.out.println("TV ON!");

    }

    /**
     *
     */
    @Override
    public void switchOff() {

        System.out.println("TV OFF!");

    }

    /**
     *
     */
    @Override
    public void irPort(){

        Integer [] numbers = {1,2,3,4,5,6,7,8,9};
        for(Integer number : numbers){
            if(number.equals(7)){
                System.out.println("IR Port Switched!");
                switchOn();
            }
        }
    }

    /**
     *
     */
    @Override
    public void up() {

        System.out.println("Volumme UP!");

    }

    /**
     *
     */
    @Override
    public void down() {

        System.out.println("Volume DOWN!");
    }
}
