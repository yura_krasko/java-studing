package org.hillel_Elem.interfaces;

import com.sun.rowset.internal.SyncResolverImpl;

import java.util.Comparator;

public class Fruit implements Comparable<Fruit> {

    private String fruitName;
    private String fruitDesc;
    private int quantity;



    public Fruit(String fruitName, String fruitDesc, int quantity){

        this.fruitName = fruitName;
        this.fruitDesc = fruitDesc;
        this.quantity = quantity;
    }


    public String getFruitName() {
        return fruitName;
    }

    public void setFruitName(String fruitName) {
        this.fruitName = fruitName;
    }

    public String getFruitDesc() {
        return fruitDesc;
    }

    public void setFruitDesc(String fruitDesc) {
        this.fruitDesc = fruitDesc;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }



    //Сравнение

    /**
     *
     * @param compareFruit
     * @return
     */
    @Override
    public int compareTo(Fruit compareFruit) {

        int compareQuantity = compareFruit.getQuantity();
        //ascending order
//        return  this.quantity - compareQuantity;
        //descendding order
        return compareQuantity - this.quantity;
    }


    /**
     * Анонимный клас
     *
     */
    public  static Comparator<Fruit> fruitNameComparator = new Comparator<Fruit>() {
        @Override
        public int compare(Fruit fruit1, Fruit fruit2) {

            String fruitName1 = fruit1.getFruitName().toUpperCase();
            String fruitname2 = fruit2.getFruitName().toUpperCase();

            //ascending order
            return fruit1.compareTo(fruit2);

            //descending order\

        }
    };
}
