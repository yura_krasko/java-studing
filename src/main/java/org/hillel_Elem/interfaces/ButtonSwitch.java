package org.hillel_Elem.interfaces;

public interface ButtonSwitch {

    void switchOn();
    void switchOff();
}
