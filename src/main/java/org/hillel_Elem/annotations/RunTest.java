package org.hillel_Elem.annotations;


import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class RunTest {

    public static void main(String[] args) {

        System.out.println("testing...");

        int passed = 0, failed = 0, count = 0, ignore = 0;

        Class<AnnotationsTest> obj = AnnotationsTest.class;

        if(obj.isAnnotationPresent(TestInfo.class)){

            Annotation annotation = obj.getAnnotation(TestInfo.class);
            TestInfo testInfo = (TestInfo) annotation;


            System.out.printf("%nPriority :%s", testInfo.priority());
            System.out.printf("%nCreateBy :%s", testInfo.createsBy());
            System.out.printf("%nTags: ");

            int tagLength = testInfo.tags().length;
            for(String tag : testInfo.tags()){

                if(tagLength > 1){

                    System.out.print(tag + ", ");
                } else{

                    System.out.print(tag);
                }

                tagLength--;
            }

            System.out.printf("%nLastModified :%s%n%n", testInfo.lastModifies());
        }

        for(Method method : obj.getDeclaredMethods()){

            if(method.isAnnotationPresent(Test.class)){
                Annotation annotation = method.getAnnotation(Test.class);
                Test test = (Test) annotation;

                if(test.enabled()){

                    try {

                        method.invoke(obj.newInstance());
                        System.out.printf("%s - genericSimpl '%s' - passed %n", ++count, method.getName());
                        passed++;

                    } catch (Throwable ex) {
                        System.out.printf("%s - genericSimpl '%s' - failed: %s %n", ++count, method.getName(), ex.getCause());
                        failed++;

                }
                }else{

                    System.out.printf("%s - genericSimpl '%s' - ignored%n", ++count, method.getName() );
                    ignore++;
                }
            }
        }

        System.out.printf("%nResult : Total : %d, Passed: %d, Failed %d, Ignore %d%n", count, passed, failed, ignore);
    }
}
