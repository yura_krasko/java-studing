package org.hillel_Elem.annotations;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

public class People implements Serializable, Cloneable {

    private String name;
    private int age;
    public int sum;
    protected int test;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    private void test(){

    }

    void test2(){

    }

    /**
     *
     * @param params
     */
    @Autowired
    @Deprecated
    protected static void method(String [] params){

    }

    /**
     *
     * @return
     */
    @Autowired
    @Override
    public String toString(){
        return super.toString();
    }
}
