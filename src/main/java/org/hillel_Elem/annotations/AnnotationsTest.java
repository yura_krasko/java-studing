package org.hillel_Elem.annotations;

@TestInfo(
        priority = TestInfo.Priority.HIGH,
        createsBy =  "andrey",
        tags = {"sales", "test"}

)
public class AnnotationsTest {

    @Test
    void testA(){

        if(true){
            throw  new RuntimeException("This test always failed");
        }
    }

    @Test(enabled = false)
    void testB(){
        if(false){
            throw  new RuntimeException("This test always passed");
        }
    }

    @Test(enabled = true)
    void testC(){
        if(10 > 1){

        }
    }
}

