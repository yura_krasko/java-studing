package org.hillel_Elem.homeworkmultithreading.taskfirst;

public class Egg implements Runnable {

    String threadName;

    public Egg(String threadName) {
        this.threadName = threadName;
    }

    @Override
    public void run() {

        System.out.println(threadName + " starting...");

        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(100);
                System.out.println("Egg");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(threadName + " finish!");
    }
}
