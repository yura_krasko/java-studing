package org.hillel_Elem.homeworkmultithreading.taskfirst;


public class Main {
    public static void main(String[] args) {

        Egg egg = new Egg("Egg");
        Thread threadEgg = new Thread(egg);
        threadEgg.start();

        Thread.currentThread().setName("Chicken");
        System.out.println(Thread.currentThread().getName() + " starting...");

        for (int i = 0; i < 10 ; i++) {
            try {
                Thread.sleep(100);
                System.out.println("Chicken");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(Thread.currentThread().getName() + " finish!");

        if (threadEgg.isAlive()){
            try {
                threadEgg.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("\nFirst appeared EGG!");
        } else {
            System.out.println("\nFirst appeared CHICKEN!");
        }
    }
}
