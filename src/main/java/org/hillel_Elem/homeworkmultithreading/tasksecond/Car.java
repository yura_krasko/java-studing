package org.hillel_Elem.homeworkmultithreading.tasksecond;

import java.util.concurrent.Callable;

public class Car implements Callable<Double>{

    private static final Integer VOLUME = 100;
    Integer carNumber;

    public Car(Integer countCar) {
        this.carNumber = countCar;
    }

    @Override
    public synchronized Double call(){

        double rezult = 0;

        for (int i = 1; i <= carNumber; i++) {

            rezult += (double) 1/i;
        }

        System.out.print(Thread.currentThread().getName() + "--->");

        return rezult * VOLUME;
    }
}

