package org.hillel_Elem.homeworkreflection;


import org.hillel_Elem.reports.InvoiceDesign;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;

/**
 * Reflection class System and class InvoiceDecign
 */

public class Reflection {

    public static void main(String[] args) {

        Class<System> systemClass = System.class;
        Class<InvoiceDesign> invoiceDesignClass = InvoiceDesign.class;

        //Package
        System.out.println("Packages: ");
        Package packageSystem = systemClass.getPackage();
        Package packageInvoiceDesign = invoiceDesignClass.getPackage();
        System.out.println("Package class System: " + packageSystem);
        System.out.println("Package class InvoiceDesign: " + packageInvoiceDesign);

        //Constructor
        System.out.println("\nConstructors: ");
        Constructor[] constructorsSystem = systemClass.getDeclaredConstructors();
        Constructor[] constructorsInvoiceDesign = invoiceDesignClass.getConstructors();
        for(Constructor constructorSystem : constructorsSystem){
            for(Constructor constructorInvoiceDesign : constructorsInvoiceDesign){

                System.out.println("Constructor class System: " + constructorSystem);
                System.out.println("Constructor class InvoiceDesign: " + constructorInvoiceDesign);
            }
        }

        //Methods
        System.out.println("\nMethods: ");
        Method[] methodsSystem = systemClass.getDeclaredMethods();
        Method[] methodsInvoicedesign = invoiceDesignClass.getDeclaredMethods();
        System.out.println ("All methods for class System: ");
        for (Method methodSystem : methodsSystem){

            System.out.print("Method name: " + methodSystem.getName() + "(");
            System.out.print("return type - " + methodSystem.getReturnType());
            Parameter[] parametersSystem = methodSystem.getParameters();
            for (Parameter parameterSystem : parametersSystem){

                System.out.print (", parameter - " + parameterSystem.getName());
                System.out.print(", type parameter - " + parameterSystem.getType());

            }

            Annotation[] annotationsSystem = methodSystem.getDeclaredAnnotations();
            for (Annotation annotationSystem : annotationsSystem){

                System.out.print(", annotation - " + annotationSystem.annotationType().getSimpleName());
            }

            System.out.println(")");
        }

        System.out.println ("All methods for class InvoiceDesign: ");
        for (Method methodInvoiceDesign : methodsInvoicedesign){

            System.out.print("Method name: " + methodInvoiceDesign.getName() + "(");
            System.out.print("return type - " + methodInvoiceDesign.getReturnType());
            Parameter[] parametersInvoiceDesign = methodInvoiceDesign.getParameters();
            for (Parameter parameterInvoiceDesign : parametersInvoiceDesign){

                System.out.print (", parameter - " + parameterInvoiceDesign.getName());
                System.out.print(", type parameter - " + parameterInvoiceDesign.getType());
            }

            Annotation[] annotationsInoiceDesign = methodInvoiceDesign.getDeclaredAnnotations();
            for (Annotation annotationInvoiceDesign : annotationsInoiceDesign){

                System.out.print(", annotation - " + annotationInvoiceDesign.annotationType().getSimpleName());
            }

            System.out.println(")");
        }

        //Fields
        System.out.println("\nFields: ");
        Field[] fieldsSystem = systemClass.getDeclaredFields();
        Field[] fieldsInvoiceDesign = invoiceDesignClass.getDeclaredFields();
        System.out.println("Fields class System: ");
        for (Field fieldSystem : fieldsSystem){

            System.out.print(fieldSystem.getName() + " - ");
            System.out.println(fieldSystem.getType());
        }

        System.out.println("Fields class InvoiceDesign: ");
        for (Field fieldInvoicedesign : fieldsInvoiceDesign){

            System.out.print(fieldInvoicedesign.getName() + " - ");
            System.out.println(fieldInvoicedesign.getType());
        }
    }
}
