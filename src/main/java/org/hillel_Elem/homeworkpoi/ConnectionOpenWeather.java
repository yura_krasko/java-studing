package org.hillel_Elem.homeworkpoi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ConnectionOpenWeather {

    /**
     * Get json from OpenWeather
     *
     * @param url
     * @return
     */
    public String getData(String url){

        String json = null;

        try {
            URL myUrl = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) myUrl.openConnection();
            connection.setRequestMethod("GET");
            BufferedReader reader=  new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String input;
            while ((input = reader.readLine()) != null){

                json = input;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }
}
