package org.hillel_Elem.homeworkpoi;

import org.apache.poi.xssf.usermodel.*;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class CreateExcel {


    /**
     * Parse responce from OpenWeather and write rezult in file
     * @param json
     */
    public void parseJsonAndWriteExcel(String json) {

        ModelData modelData = new ModelData();
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Test weather");
        sheet.setDefaultColumnWidth(10);
        XSSFDataFormat dateFormat = workbook.createDataFormat();

        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(10);
        font.setColor(XSSFFont.COLOR_RED);

        //Style rowHeader
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(XSSFCellStyle.ALIGN_CENTER);

        //Style row
        XSSFCellStyle styleRow = workbook.createCellStyle();
        styleRow.setAlignment(XSSFCellStyle.ALIGN_CENTER);

        XSSFRow rowHeader = sheet.createRow(0);
        rowHeader.createCell(0).setCellValue("contry");
        rowHeader.createCell(1).setCellValue("city");
        rowHeader.createCell(2).setCellValue("data");
        rowHeader.createCell(3).setCellValue("dayTemp");
        rowHeader.createCell(4).setCellValue("nightTemp");
        rowHeader.createCell(5).setCellValue("Description");
        rowHeader.setRowStyle(style);
        rowHeader.setHeightInPoints(30);

        JSONObject obj = new JSONObject(json);
        JSONObject jsonObject = obj.getJSONObject("city");
        modelData.setName(jsonObject.getString("name"));
        modelData.setCountry(jsonObject.getString("country"));

        JSONArray jsonArray = obj.getJSONArray("list");
        for (int i = 0; i < jsonArray.length(); i++){

            JSONObject obj1 = jsonArray.getJSONObject(i);
            modelData.setDate(obj1.getLong("dt"));

            JSONObject obj3 = obj1.getJSONObject("temp");
            modelData.setDayTemp(obj3.getDouble("day"));
            modelData.setNightTemp(obj3.getDouble("night"));

            JSONArray arr2 = obj1.getJSONArray("weather");
            for (int j = 0; j < arr2.length(); j++){

                JSONObject obj5 = arr2.getJSONObject(j);
                modelData.setDescription(obj5.getString("description"));
            }

            XSSFRow row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue(modelData.getCountry());
            row.createCell(1).setCellValue(modelData.getName());
            row.createCell(2).setCellValue(modelData.getDate());
            row.createCell(3).setCellValue(modelData.getDayTemp());
            row.createCell(4).setCellValue(modelData.getNightTemp());
            row.createCell(5).setCellValue(modelData.getDescription());
            row.setRowStyle(styleRow);
        }

        try(FileOutputStream out = new FileOutputStream(new File("test.xlsx"))){
            workbook.write(out);
            out.flush();
            System.out.println("Success!!!");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
