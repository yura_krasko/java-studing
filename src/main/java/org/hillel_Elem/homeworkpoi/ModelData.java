package org.hillel_Elem.homeworkpoi;

import java.util.Calendar;

public class ModelData {

    private String name;
    private String country;
    private Long date;
    private Double dayTemp;
    private Double nightTemp;
    private String description;

    public ModelData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDate() {

        Calendar mydate = Calendar.getInstance();
        mydate.setTimeInMillis(date*1000);
        return  mydate.get(Calendar.DAY_OF_MONTH)+"."+
                mydate.get(Calendar.MONTH)+"."+
                mydate.get(Calendar.YEAR);


        //return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getDayTemp() {
        return dayTemp - 273;
    }

    public void setDayTemp(Double dayTemp) {
        this.dayTemp = dayTemp;
    }

    public Double getNightTemp() {
        return nightTemp - 273;
    }

    public void setNightTemp(Double nightTemp) {
        this.nightTemp = nightTemp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

