package org.hillel_Elem.serialization;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

    private static  final long serialVersionUID = 863482637846287236L;

    private String firstName;
    private String lastName;
    private int accountNumber;
    private Date dateOpened;

    public User(String firstName, String lastName, int accountNumber, Date dateOpened) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountNumber = accountNumber;
        this.dateOpened = dateOpened;
    }

    public User() {
        super();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Date getDateOpened() {
        return dateOpened;
    }

    public void setDateOpened(Date aNewdate) {
        Date newDate = new Date(aNewdate.getTime());
        dateOpened = newDate;
    }

    private void  readObject(ObjectInputStream aInputStream) throws IOException {
        firstName = aInputStream.readUTF();
        lastName = aInputStream.readUTF();
        accountNumber = aInputStream.readInt();
        dateOpened = new Date(aInputStream.readLong());
    }

    private void  writeObject(ObjectOutputStream aOutpetStream) throws IOException {
        aOutpetStream.writeUTF(firstName);
        aOutpetStream.writeUTF(lastName);
        aOutpetStream.writeInt(accountNumber);
        aOutpetStream.writeLong(dateOpened.getTime());
    }
}
