package org.hillel_Elem.generics;

import java.util.Collection;

public class GenericSimpl<T> {

    void test(T t){
        System.out.println(t.toString());
    }

    public static <T> T addAndReturn(T element, Collection<T> collection){
        collection.add(element);
        return element;
    }
}
