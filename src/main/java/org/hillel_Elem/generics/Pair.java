package org.hillel_Elem.generics;

public class Pair<KeyType, ValueType> {

    private KeyType key;
    private ValueType value;

    public Pair(KeyType key, ValueType value) {
        this.key = key;
        this.value = value;
    }

    public KeyType getKey() {
        return key;
    }

    public void setKey(KeyType key) {
        this.key = key;
    }

    public ValueType getValue() {
        return value;
    }

    public void setValue(ValueType value) {
        this.value = value;
    }

    public static void main(String[] args) {

        Pair<Integer, String> p = new Pair<>(1, "A");

        System.out.println(p.getKey().getClass().getName());
    }
}
