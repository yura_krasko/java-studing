package org.hillel_Elem.generics;

public class Dog extends Animal {

    private String dogName;

    @Override
    public Dog name(Object o) {
        this.dogName = (String)o;
        return this;
    }

    @Override
    Object getData() {
        return dogName;
    }
}
