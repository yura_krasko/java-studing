package org.hillel_Elem.reports;

import com.sun.org.apache.xerces.internal.impl.xpath.XPath;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InvoiceData {

    private Invoice invoice;

    public InvoiceData(){

        invoice = createInvoice();
    }

    /**
     *
     * @return
     */
    private Invoice createInvoice(){

        Invoice invoice = new Invoice();
        invoice.setId(5);
        invoice.setShippoing(new BigDecimal(10));
        invoice.setTax(0.2);

        invoice.setBillTo(createCustomer("Mary Patterson", "151 Pomton St.", "Washigton", "mpatteson@noemail.com"));
        invoice.setShipTo((createCustomer("Peter Marsh", "23 Baden av.", "New Yourk", null)));

        List<Item> items = new ArrayList<>();
        items.add(createItems("Notebook", 1, new BigDecimal(1000)));
        items.add(createItems("DVD", 5, new BigDecimal(40)));
        items.add(createItems("Book", 2, new BigDecimal(10)));
        items.add(createItems("Phone", 1, new BigDecimal(200)));
        invoice.setItems(items);

        return invoice;
    }

    /**
     *
     * @param name
     * @param address
     * @param city
     * @param email
     * @return
     */
    private Customer createCustomer(String name, String address, String city, String email){

        Customer customer = new Customer();
        customer.setName(name);
        customer.setAddress(address);
        customer.setCity(city);
        customer.setEmail(email);

        return customer;
    }

    /**
     *
     * @param description
     * @param quantity
     * @param unitprice
     * @return
     */
    private Item createItems(String description, Integer quantity, BigDecimal unitprice){

        Item item = new Item();
        item.setDescription(description);
        item.setQuantity(quantity);
        item.setUnitprice(unitprice);

        return item;
    }

    public Invoice getInvoice(){

        return invoice;
    }

    /**
     *
     * @return
     */
    public JRDataSource createDataSource(){

        return  new JRBeanCollectionDataSource(invoice.getItems());
    }
}
