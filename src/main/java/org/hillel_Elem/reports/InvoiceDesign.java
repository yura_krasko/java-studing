package org.hillel_Elem.reports;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.base.expression.AbstractSimpleExpression;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.HorizontalListBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.builder.subtotal.AggregationSubtotalBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.definition.ReportParameters;

import java.math.BigDecimal;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

public class InvoiceDesign {

    private InvoiceData data = new InvoiceData();

    public InvoiceDesign(InvoiceData data){
        this.data = data;
    }

    private AggregationSubtotalBuilder<BigDecimal> totalSum;

    public JasperReportBuilder build() throws Exception{

        JasperReportBuilder report = report();
        //init style
        StyleBuilder columnStyle = stl.style(Templates.columnStyle)
                .setBorder(stl.pen1Point());
        StyleBuilder subtotalStyle = stl.style(columnStyle)
                .bold();
        StyleBuilder shippingStyle = stl.style(Templates.boldStyle)
                .setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);

        //init columns
        TextColumnBuilder<Integer> rowNumberColumn = col.reportRowNumberColumn()
                .setFixedColumns(2)
                .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        TextColumnBuilder<String> descriptionColumn = col.column("Description", "description", type.stringType())
                .setFixedWidth(250);
        TextColumnBuilder<Integer> quantityColumn = col.column("Quantity", "quantity", type.integerType())
                .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        TextColumnBuilder<BigDecimal> unitPriceColimn = col.column("Unit Price", "unitprice", Templates.currencyType);
        TextColumnBuilder<String> taxtColumn = col.column("Tax", exp.text("20%"))
                .setFixedColumns(3);

        //price = unitPrice * quality
        TextColumnBuilder<BigDecimal> priceColumn = unitPriceColimn.multiply(quantityColumn)
                .setTitle("Price")
                .setDataType(Templates.currencyType);

        //vat = price * tag
        TextColumnBuilder<BigDecimal> vatColumn = priceColumn.multiply(data.getInvoice().getTax())
                .setTitle("Vat")
                .setDataType(Templates.currencyType);

        //total = priec + vate
        TextColumnBuilder<BigDecimal> totalColumn = priceColumn.add(vatColumn)
                .setTitle("Total Price")
                .setDataType(Templates.currencyType)
                .setRows(2)
                .setStyle(subtotalStyle);

        //init subtotal
        totalSum = sbt.sum(totalColumn)
                .setLabel("Total: ")
                .setLabelStyle(Templates.boldStyle);

        //configure report
        report
                .setTemplate(Templates.reportTemplate)
                .setColumnStyle(columnStyle)
                .setSubtotalStyle(subtotalStyle)
                //columns
                .columns(
                        rowNumberColumn, descriptionColumn, quantityColumn, unitPriceColimn, totalColumn, priceColumn, taxtColumn, vatColumn)
                .columnGrid(
                        rowNumberColumn, descriptionColumn, quantityColumn, unitPriceColimn,
                        grid.horizontalColumnGridList()
                            .add(totalColumn).newRow()
                            .add(priceColumn, taxtColumn, vatColumn))
                //subtotal
                .subtotalsAtSummary(
                        totalSum, sbt.sum(priceColumn), sbt.sum(vatColumn))
                //hand components
                .title(
                        Templates.createTitleComponent("Invoice No: " + data.getInvoice().getId()),
                        cmp.horizontalList().setStyle(stl.style(10)).setGap(50).add(
                                cmp.hListCell(createCustomerComponent("Bill to", data.getInvoice().getBillTo())).heightFixedOnTop(),
                                cmp.hListCell(createCustomerComponent("Ship to", data.getInvoice().getShipTo())).heightFixedOnTop()),
                        cmp.verticalGap(10))
                        .pageFooter(
                                Templates.footerComponent)
                        .summary(
                                cmp.text(data.getInvoice().getShippoing()).setValueFormatter(Templates.createCurrencyValueFormatter("Shipping: ")).setStyle(shippingStyle),
                                cmp.horizontalList(
                                        cmp.text("Payment terms: 30 days").setStyle(Templates.bold12CenteredStyle),
                                        cmp.text(new TotalPaymentExpression()).setStyle(Templates.bold12CenteredStyle)),
                                cmp.verticalGap(30),
                                cmp.text("Thank you for your business").setStyle(Templates.bold12CenteredStyle))
                        .setDataSource(data.createDataSource());


                return report;
    }

    private VerticalListBuilder createCustomerComponent(String label, Customer customer){

        HorizontalListBuilder list = cmp.horizontalList().setBaseStyle(stl.style().setTopBorder(stl.pen1Point()).setLeftPadding(10));
        addCustomerAttribute(list, "Name", customer.getName());
        addCustomerAttribute(list, "Address", customer.getAddress());
        addCustomerAttribute(list, "City", customer.getCity());
        addCustomerAttribute(list, "Email", customer.getEmail());
        return cmp.verticalList(
                cmp.text(label).setStyle(Templates.boldStyle),
                list);

    }

    private void  addCustomerAttribute(HorizontalListBuilder list, String label, String value){
        if(value != null){
            list.add(cmp.text(label + ":").setFixedColumns(8).setStyle(Templates.boldStyle), cmp.text(value)).newRow();
        }
    }

    private class TotalPaymentExpression extends AbstractSimpleExpression<String>{

        private static final long serialversionUID = 1L;

        @Override
        public String evaluate(ReportParameters reportParameters) {
            BigDecimal total = reportParameters.getValue(totalSum);
            BigDecimal shipping = total.add(data.getInvoice().getShippoing());
            return "Total payment: " + Templates.currencyType.valueToString(shipping, reportParameters.getLocale());
        }
    }




}
