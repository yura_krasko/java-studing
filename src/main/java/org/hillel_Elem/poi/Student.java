package org.hillel_Elem.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Student {

    public static void main(String[] args) {

        Map<String, Object[]> student = new TreeMap<>();
        student.put("1", new Object[]{"Student", "Age", "Faculty"});
        student.put("2", new Object[]{"Yura", "25", "Java"});
        student.put("3", new Object[]{"Vasia", "23", "QA"});
        student.put("4", new Object[]{"Olia", "26", "PHP"});

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Students");
        XSSFRow row;

        Set<String> keyid = student.keySet();
        int rowid = 0;
        for (String key : keyid){

            row = sheet.createRow(rowid++);
            Object[] objects = student.get(key);
            int cellid = 0;
            for (Object obj : objects){

                Cell cell = row.createCell(cellid++);
                cell.setCellValue((String) obj);
            }
        }

        try(FileOutputStream out = new FileOutputStream(new File("Students.xlsx"))){

            workbook.write(out);
            out.flush();
            System.out.println("Write success");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    public static void createStudent(){

        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = workbook.createSheet("Students");

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Name");
        header.createCell(1).setCellValue("Age");
        header.createCell(2).setCellValue("Faculty");

        Row dataRow = sheet.createRow(1);
        dataRow.createCell(0).setCellValue("Yura");
        dataRow.createCell(1).setCellValue("25");
        dataRow.createCell(2).setCellValue("java");

        try(FileOutputStream out = new FileOutputStream(new File("test.xlsx"))){

            workbook.write(out);
            out.flush();
            System.out.println("Succes");

        }  catch (IOException e) {
            e.printStackTrace();
        }

    }

}
