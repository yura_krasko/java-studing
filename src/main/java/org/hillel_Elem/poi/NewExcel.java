package org.hillel_Elem.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class NewExcel {

    public static void main(String[] args) {

        //Create a blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();
        //Create a blank shhet
        XSSFSheet spreadsheet = workbook.createSheet("Employee Info ");
        //Create row Object
        XSSFRow row;
        //This data needs to be written(Object[])
        Map<String, Object[]> empiunfo = new TreeMap<>();

        empiunfo.put("1", new Object[]{"EMP ID", "EMP NAME", "DESIGNATION"});
        empiunfo.put("2", new Object[]{"tp1", "Gopal", "Technical Manager"});
        empiunfo.put("3", new Object[]{"tp2", "Manisha", "Proog Reader"});
        empiunfo.put("4", new Object[]{"tp3", "Mashtan", "Technical Writer"});
        empiunfo.put("5", new Object[]{"tp4", "Satish", "Technical Writer"});
        empiunfo.put("6", new Object[]{"tp5", "Krishna", "Technical Writer"});

        //Iterate over data and write to sheet
        Set<String> keyid = empiunfo.keySet();

        int rowid = 0;
        for (String key : keyid){

            row = spreadsheet.createRow(rowid++);
            Object [] object = empiunfo.get(key);
            int cellid = 0;
            for (Object obj : object){

                Cell cell  =row.createCell(cellid++);
                cell.setCellValue((String) obj);
            }
        }
        //spreadsheet.autoSizeColumn(rowid);
        //Write the workbook in file system

        try{
            FileOutputStream out = new FileOutputStream(new File("Writesheet.xlsx"));
            workbook.write(out);
            out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Writesheet.xlsx written succesfully");
    }
}
