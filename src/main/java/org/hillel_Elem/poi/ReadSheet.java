package org.hillel_Elem.poi;



import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.util.Iterator;

public class ReadSheet {

    static XSSFRow row;

    public static void main(String[] args) {

        try(
            FileInputStream fis = new FileInputStream(new File("/home/yura/IdeaProjects/hillel_Elem/Writesheet.xlsx"))){

                XSSFWorkbook workbook = new XSSFWorkbook(fis);

                XSSFSheet spreadsheet = workbook.getSheetAt(0);

                Iterator<Row> rowIterator =  spreadsheet.iterator();

                while (rowIterator.hasNext()) {

                    row  = (XSSFRow) rowIterator.next();
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while(cellIterator.hasNext()){

                        Cell cell = cellIterator.next();
                        switch (cell.getCellType()){
                            case Cell.CELL_TYPE_NUMERIC :
                                System.out.print(cell.getNumericCellValue() + "\t\t");
                                break;
                            case Cell.CELL_TYPE_STRING :
                                System.out.print(cell.getStringCellValue() + "\t\t");
                                break;
                        }
                    }

                    System.out.println();
                }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
