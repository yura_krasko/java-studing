package org.hillel_Elem.functional;

@FunctionalInterface
public interface ComplexFunctionalInterface extends SimpleFuncInterface {

    static void doSomWork(){
        System.out.println("Doing some work in interface impl....");
    }

    default void doSomeOtherWork(){
        System.out.println("Doing some other work in interface impl....");
    }

    @Override
    void  doWork();

    @Override
    String toString();

    @Override
    boolean equals(Object o);
}
