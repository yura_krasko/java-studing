package org.hillel_Elem.functional;

@FunctionalInterface
public interface SimpleFuncInterface {

    void  doWork();
    String toString();
    boolean equals(Object o);

}
