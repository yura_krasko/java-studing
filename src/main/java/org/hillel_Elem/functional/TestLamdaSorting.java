package org.hillel_Elem.functional;

import java.util.ArrayList;
import java.util.List;

public class TestLamdaSorting {

    public static void main(String[] args) {

        List<Developer> listDev = getDeveloper();
        System.out.println("Before sort");
        for (Developer dev : listDev){
            System.out.println(dev);
        }

        System.out.println("After sort");
        //lamda here
        listDev.sort((o1, o2) -> o1.getAge() - o2.getAge());
        //java 8 only, lamda also, to print the list
        listDev.forEach((developer) -> System.out.println(developer));
    }

    private static List<Developer> getDeveloper(){

        List<Developer> rezult = new ArrayList<>();
        rezult.add(new Developer("yura", 10000, 25));
        rezult.add(new Developer("alex", 17000, 45));
        rezult.add(new Developer("alvin", 1000, 35));
        rezult.add(new Developer("iris", 10000, 17));

        return rezult;
    }
}
