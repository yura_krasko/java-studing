package org.hillel_Elem.functional;

import java.util.*;

public class TestRegularString {

    public static void main(String[] args) {

        List<Developer> listDev = getDeveloper();
        System.out.println("Before sort");
        for (Developer dev : listDev){
            System.out.println(dev);
        }

        Collections.sort(listDev, new Comparator<Developer>() {
            @Override
            public int compare(Developer o1, Developer o2) {
                return o1.getAge() - o2.getAge();
            }
        });

        System.out.println("After sort");
        for (Developer dev : listDev){
            System.out.println(dev);
        }

    }

    /**
     *
     * @return
     */
    private static List<Developer> getDeveloper(){

        List<Developer> rezult = new ArrayList<>();
        rezult.add(new Developer("yura", 10000, 25));
        rezult.add(new Developer("alex", 17000, 45));
        rezult.add(new Developer("alvin", 1000, 35));
        rezult.add(new Developer("iris", 10000, 17));

        return rezult;
    }
}
