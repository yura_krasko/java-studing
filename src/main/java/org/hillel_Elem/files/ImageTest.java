package org.hillel_Elem.files;

import javafx.scene.paint.Color;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageTest {

    public static void main(String[] args) {

        File firstImage = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/image1.png");
        File secondImage = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/image2.png");

        try {
            BufferedImage firstBufferImage = ImageIO.read(firstImage);
            BufferedImage secondBufferImage = ImageIO.read(secondImage);

            File outputFile = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/diggerencs.png");
            ImageIO.write(compareTwoImages(firstBufferImage, secondBufferImage), "png", outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

        public static  BufferedImage compareTwoImages(BufferedImage firstImage, BufferedImage secondImage) {

            for (int y = 0; y < firstImage.getHeight(); y++) {
                for (int x = 0; x < firstImage.getWidth(); x++) {

                    int clr = firstImage.getRGB(x, y);

                    int lll = secondImage.getRGB(x, y);

                    if (clr != lll) {

                        secondImage.setRGB(x, y, java.awt.Color.RED.getRGB());
                    }
                }
            }
            return secondImage;
        }


}
