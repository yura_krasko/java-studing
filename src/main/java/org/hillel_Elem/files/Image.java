package org.hillel_Elem.files;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Image {

    public static void main(String[] args) {

        File firstImege = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/1.png");
        File secondImage = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/2.png");
        File thredImage = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/3.png");
        File fourthImage = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/4.png");

        try {

            BufferedImage firstBuffer = ImageIO.read(firstImege);
            BufferedImage secondBuffer = ImageIO.read(secondImage);
            BufferedImage thrredBuffer = ImageIO.read(thredImage);
            BufferedImage fourthBuffer = ImageIO.read(fourthImage);

            File rezult = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/testw1.png");
            ImageIO.write(compareTwoImage(firstBuffer, secondBuffer), "png", rezult);

            File rezult1 = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/test2.png");
            ImageIO.write(compareTwoImage(firstBuffer, thrredBuffer), "png", rezult1);

            File rezult2 = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/test3.png");
            ImageIO.write(compareTwoImage(firstBuffer, fourthBuffer), "png", rezult2);

            File rezult3 = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/test4.png");
            ImageIO.write(compareTwoImage(secondBuffer, thrredBuffer), "png", rezult3);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static BufferedImage compareTwoImage(BufferedImage firstImage, BufferedImage secondImage){

        for (int x = 0; x < firstImage.getHeight(); x++){
            for (int y = 0; y < firstImage.getWidth(); y++){

                int clr = firstImage.getRGB(y, x);
                int lll = secondImage.getRGB(y, x);

                if(clr != lll){
                    secondImage.setRGB(y, x, Color.RED.getRGB());
                }
            }
        }
        return secondImage;
    }

}
