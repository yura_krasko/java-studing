package org.hillel_Elem.files;

import java.io.*;

public class FileExample{

    public static void main(String[] args) {

        final String PATH = "/home/yura/IdeaProjects/hillel_Elem/src/main/resources/files_data/";

        File file;
        File[] files = null;
        String str = "";
        try{

            for (int i = 0; i < 15; i++){

                file = new File(PATH + "Test" + i + ".txt");
                System.out.println(file.createNewFile());
                System.out.println(file.getName());
                System.out.println(file.toString());
                System.out.println(file.canExecute());
                System.out.println(file.canWrite());
                System.out.println(file.delete());
                System.out.println(file.createNewFile());
                System.out.println(file.getParent());
            }

            File folder = new File(PATH);
            files = folder.listFiles();

            for (File everyFile : files){
              System.out.println(everyFile);
             }
        } catch (IOException e) {
            e.printStackTrace();
        }

        File inputFile = new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/Song.txt");
        System.out.println(inputFile.length());
        if (inputFile.exists() && inputFile.length() > 0){

            try(FileInputStream fileInputStream = new FileInputStream(inputFile)){

                byte[] bytes = new byte[(int) inputFile.length()];
                int x = fileInputStream.read(bytes);
                System.out.println(x);

                char symbol;
                for (byte b : bytes){

                    symbol = (char) b;
                    System.out.print(symbol);
                    str += symbol;
                }

                System.out.println();
                System.out.println(str);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        boolean toWrite = false;
        boolean toModify = false;

        if (files != null){
            for (File everyFile : files){
                if(everyFile.length() == 0){
                    toWrite = true;
                } else{
                    toModify = true;
                }

                if (toWrite){
                    try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(everyFile))){

                        objectOutputStream.writeObject(str);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if(toModify)
                    try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(everyFile, true))) {

                        objectOutputStream.writeObject("THE BEATLES");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }

    }


}
