package org.hillel_Elem.inheritance;

public class Square {

    private double side = 1.0;

    public Square(){
           }

     public Square(double side){
        this.side = side;
     }

     public void setSide(double side){
         this.side = side;
     }

     public double getSide(){
         return side;
     }


     public double calculateArea(){
         return this.side * this.side;
     }

     public double getArea(){
         return  calculateArea();
     }

     @Override
    public String toString(){
         return "The side of the Square " + side + ", and the area is "
                 + getArea();

     }


}
