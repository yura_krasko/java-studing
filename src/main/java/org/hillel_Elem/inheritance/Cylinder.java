package org.hillel_Elem.inheritance;

public class Cylinder extends Circle {

    private  double height;

    //Constructor with defoult radius and height
    public Cylinder(){
        super();
        height = 1.0;
    }

    //Constructor with defoult radius, but given height
    public Cylinder( double height) {
        super();
        this.height = height;
    }

    //Constructor with given radius, height
    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    //A public method for retrieving the height
    public double getHeight(){
        return height;
    }

    //A public method for computing the volume of cylinder
    //use seperclass method getArea to get the base rea
    public  double getVolume(){
        return  getArea() * height;
    }


    @Override
    public String toString() {
        return "Cylinder: subclass of " + super.toString()
                + " height : " + height;
    }
}
