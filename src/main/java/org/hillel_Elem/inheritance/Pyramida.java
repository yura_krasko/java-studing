package org.hillel_Elem.inheritance;

public class Pyramida extends Triangle{

    private double height1;

    public Pyramida(){
        height1 = 1.0;
    }

    public Pyramida(double height1){
        super();
        this.height1 = height1;
    }

    public Pyramida(double base, double height, double height1){
        super(base,height);
        this.height1 = height1;
    }

    public void setHeight1(double height1){
        this.height1 = height1;
    }

    public double getHeight1(){
        return  height1;
    }

    private double calculate(){
        return (getArea() * height1) / 3;
    }

    public double getVolume(){
        return calculate();
    }

    @Override
    public String toString() {
        return "Pyramide: subclass " + super.toString()
                + " height " + height1;
    }
}
