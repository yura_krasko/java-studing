package org.hillel_Elem.inheritance;

import com.sun.org.apache.regexp.internal.RE;

public class Paralelogram extends Retangle{


        private double kut;


    public Paralelogram(){
            kut = 0;
        }

        public Paralelogram(double kut){
            this.kut = kut;
        }

        public  Paralelogram(double sideSecond, double kut){
            super(sideSecond);
            this.kut = kut;
        }

        public  Paralelogram(double side, double sideSecond, double kut){
            super(side, sideSecond);
            this.kut = kut;
        }

        public void setKut(double kut){
            this.kut = kut;
        }



        public double  getKut(){
            return kut;
        }

        public double getAreaPalal(){

            return getAreaRetan() * Math.sin(Math.toRadians(kut));
        }

        @Override
        public String toString() {
            return "Pararlelogram: subclass of " + super.toString()
                    + " kut " + kut;
        }
}


