package org.hillel_Elem.inheritance;

public class Triangle {

    private double height;
    private double base;

    public Triangle(){
        height = 1.0;
        base = 1.0;
    }

    public Triangle(double height, double base){
        this.height = height;
        this.base = base;
    }


    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    private double calculate(){
        return (this.base*this.height) / 2;

    }

    public double getArea(){
        return calculate();
    }

    @Override
    public String toString() {
        return "The base of triangle = " + base  + " height = " + height+
                " Area = " +getArea();
    }
}
