package org.hillel_Elem.inheritance;

public class Retangle extends Square{

    private double sideSecond;

    public Retangle(){
        sideSecond = 1.0;
    }

    /**
     *
     * @param sideSecond
     */
    public  Retangle(double sideSecond){
        this.sideSecond = sideSecond;
    }


    public Retangle(double side, double sideSecond){
        super(side);
        this.sideSecond = sideSecond;
    }

    public double getSideSecond() {
        return sideSecond;
    }


    public double getAreaRetan(){
        return getSide() * sideSecond;
    }

    @Override
    public String toString() {
        return "Retangle: subclass of " + super.toString()
                + " sideSecond " + sideSecond;
    }
}
