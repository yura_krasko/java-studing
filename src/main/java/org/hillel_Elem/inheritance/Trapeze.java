package org.hillel_Elem.inheritance;

public class Trapeze extends Retangle{

    private double height;

    public Trapeze(){
        height = 1.0;
    }

    public Trapeze(double height){
        this.height = height;
    }

    public  Trapeze(double side, double height){
        super(side);
        this.height = height;

    }

    public Trapeze(double side, double sideSecond, double height){
        super(side, sideSecond);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getAreaTrap (){

        return ((getSide() + getSideSecond()) *height)/2;
    }

    @Override
    public String toString() {
        return "Trapeze: subclass of " + super.toString() +
                " height " + height;
    }
}
