package org.hillel_Elem.inheritance;

import org.hillel_Elem.encapsulation.Encapsulation;

public class EncapsulationTest  extends Encapsulation{

    public static void main(String[] args) {

        Encapsulation encapsulation = new Encapsulation();
        encapsulation.x2 = 25;

        EncapsulationTest encapsulationTest= new EncapsulationTest();
        encapsulationTest.x1 = 15;
        encapsulationTest.x2 = 23;

        System.out.println(encapsulationTest.x1);

    }
}
