package org.hillel_Elem.enums;

public class StatusExampleAbstract {

    public enum Status{

        STATUS_OPEN{
            public String description(){
                return "open";
            }
        },
        STATUS_STARTED{
            public String description(){
                return "started";
            }
        },
        STATUS_INPROCESS{
            public String description(){
                return "inprocess";
            }
        },
        STATUS_ONHOLD{
            public String description(){
                return "onhold";
            }
        },
        STATUS_COMPLETED{
            public String description(){
                return "completed";
            }
        },
        STATUS_CLOSED{
            public String description(){
                return "closed";
            }
        };


        Status (){
        }

        public abstract String description();

        public static void data(String value){
            for (Status status : Status.values()){
                if(status.name().equalsIgnoreCase(value)){
                    System.out.println(status.description());
                }
            }
        }

    }

    public static void main(String[] args) {

        Status.data("STATUS_CloSed");
    }
}
