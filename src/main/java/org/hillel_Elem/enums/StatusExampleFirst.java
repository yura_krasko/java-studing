package org.hillel_Elem.enums;

public class StatusExampleFirst {

    public enum Status{

        STATUS_OPEN(0),
        STATUS_STARTED(1),
        STATUS_INPROGRESS(2),
        STATUS_ONHOLD(3),
        STATUS_COMPLETED(4),
        STATUS_CLOSED(5);

        private final int status;

        Status(int aStatus){
            this.status = aStatus;
        }

        public int getStatus(){
            return this.status;
        }
    }

    public static void main(String[] args) {

        for (Status status : Status.values()){
            System.out.println(status + " value is " + status.status);
        }
    }
}
