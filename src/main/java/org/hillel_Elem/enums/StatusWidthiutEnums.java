package org.hillel_Elem.enums;

public class StatusWidthiutEnums {

    public static final int STATUS_OPEN = 0;
    public static final int STATUS_STARTED = 1;
    public static final int STATUS_INPROGRESS = 2;
    public static final int STATUS_ONHOLD = 3;
    public static final int STATUS_COMPLETED = 4;
    public static final int STATUS_CLOSE = 5;
}
