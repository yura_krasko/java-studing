package org.hillel_Elem.enums;

public class TestEnum {

    public enum Planet{
        PLANET_FIRST(1){
            public String planetName(){
                return "Mercuriy";
            }
        },
        PLANET_SECOND(2){
            public String planetName(){
                return "Venera";
            }
        },
        PLANET_THIRD(3){
            public String planetName(){
                return "Earth";
            }
        },
        PLANET_FOURTH(4){
            public String planetName(){
                return "Mars";
            }
        },
        PLANET_FIFTH(5){
            public String planetName(){
                return "Jupiter";
            }
        },
        PLANET_SIXTH(6){
            public String planetName(){
                return "Saturn";
            }
        },
        PLANET_SEVENTH(7){
            public String planetName(){
                return "Uran";
            }
        },
        PLANET_EIGHT(8){
            public String planetName(){
                return "Neptun";
            }
        };


        private final int position;

        public abstract String planetName();

        Planet(int aPosition){
            this.position = aPosition;
        }

        public int getPosition(){
            return this.position;
        }

    }

    public static void main(String[] args) {

        for (Planet plan : Planet.values()){
            System.out.println(plan + "---> "+ plan.getPosition() + " is name " + plan.planetName());
        }
    }
}
