package org.hillel_Elem.homeworkreports;

import net.sf.dynamicreports.report.base.expression.AbstractValueFormatter;
import net.sf.dynamicreports.report.builder.datatype.BigDecimalType;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.definition.ReportParameters;
import java.awt.*;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;


public class Templates {

    public static final StyleBuilder boldStyle;
    public static final StyleBuilder boldRightStyle;
    public static final StyleBuilder boldLeftStyle;
    public static final StyleBuilder underLineStyle;
    public static final StyleBuilder boldCenteredStyle;
    public static final StyleBuilder columnTitleStyle;
    public static final StyleBuilder columnStyle;
    public static final CurrencyType currencyType;
    public static final StyleBuilder subtotalStyle;
    public static final StyleBuilder headerStyle;
    public static final StyleBuilder underlineStyle;
    public static final StyleBuilder italicStyle;
    public static final StyleBuilder invoiceComponent;
    public static final StyleBuilder attributeStyle;
    public static final StyleBuilder atSumarryStyle;
    public static final StyleBuilder sumaryStyle;
    public static final StyleBuilder accontsStyle;
    public static final StyleBuilder managerStyle;
    public static final StyleBuilder footerStyle;

    static {

        boldStyle = stl.style().bold();

        boldRightStyle = stl.style(boldStyle).setHorizontalAlignment(HorizontalAlignment.RIGHT);

        boldLeftStyle = stl.style(boldStyle)
                .setFontSize(20);

        underLineStyle = stl.style().underline().setFontSize(20).setHorizontalAlignment(HorizontalAlignment.RIGHT);

        boldCenteredStyle = stl.style(boldStyle).setHorizontalAlignment(HorizontalAlignment.CENTER).setFontSize(30)
                .setPadding(25);

        columnTitleStyle = stl.style(boldCenteredStyle)
                .setTopBorder(stl.pen2Point()
                        .setLineColor(Color.BLACK)).setBottomBorder(stl.pen2Point()
                        .setLineColor(Color.BLACK)).setBackgroundColor(Color.GRAY).setFontSize(10).setPadding(10);
        columnStyle = stl.style(boldCenteredStyle).setBackgroundColor(Color.lightGray).setPadding(5).setFontSize(8).setBottomBorder(stl.pen1Point());

        subtotalStyle = stl.style(boldStyle).setBorder(stl.pen1Point()).setBackgroundColor(Color.GRAY).setFontSize(10).setPadding(10);

        currencyType = new CurrencyType();

        headerStyle = stl.style().setBorder(stl.penDotted()).setBackgroundColor(Color.LIGHT_GRAY).setPadding(7).setFontSize(13);

        underlineStyle = stl.style().underline().bold();

        italicStyle = stl.style().italic();

        invoiceComponent = stl.style().boldItalic().setFontSize(15).setLeftIndent(100);

        attributeStyle = stl.style().bold().setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                .setBottomBorder(stl.pen2Point()).setPadding(7).setFontSize(13);

        atSumarryStyle = stl.style().boldItalic().setPadding(2).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                .setBackgroundColor(Color.GRAY).setBorder(stl.pen2Point());

        sumaryStyle = stl.style().boldItalic()
                .setFontSize(15);

        accontsStyle = stl.style().italic().setForegroundColor(Color.GRAY).setRightIndent(60).setTopPadding(20)
                                    .setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT)
        ;
        managerStyle = stl.style().boldItalic().setFontSize(15).setRightIndent(30)
                .setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);

        footerStyle = stl.style().boldItalic().setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                .setFontSize(15);
    }


    /**
     *
     */
    public static class CurrencyType extends BigDecimalType {

        @Override
        public String getPattern() {
            return "$ #,###.00";
        }
    }

    /**
     *
     */
    private static class CurrencyValueFormatter extends AbstractValueFormatter<String, Number> {

        private static final long serialVersionUID = 1L;
        private String label;

        /**
         *
         * @param label
         */
        public CurrencyValueFormatter(String label) {
            this.label = label;
        }

        @Override
        public String format(Number value, ReportParameters reportParameters) {
            return label + currencyType.valueToString(value, reportParameters.getLocale());
        }
    }

}
