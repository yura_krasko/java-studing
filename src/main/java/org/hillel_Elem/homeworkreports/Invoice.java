package org.hillel_Elem.homeworkreports;

import java.util.List;

public class Invoice {

   private List<Item> invoice;
   private Company sender;
   private Company receiver;
   private Integer id;
   private String account;
   private String invoiceData;
   private Double tax;

    public List<Item> getInvoice() {
        return invoice;
    }

    public void setInvoice(List<Item> invoice) {
        this.invoice = invoice;
    }

    public Company getSender() {
        return sender;
    }

    public void setSender(Company sender) {
        this.sender = sender;
    }

    public Company getReceiver() {
        return receiver;
    }

    public void setReceiver(Company receiver) {
        this.receiver = receiver;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getInvoiceData() {
        return invoiceData;
    }

    public void setInvoiceData(String invoiceData) {
        this.invoiceData = invoiceData;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }
}
