package org.hillel_Elem.homeworkreports;

public class Company {

    private String companyName;
    private String address;
    private String zipCode;
    private String telephone;
    private String email;

    public Company() {
    }

    /**
     *
     * @param companyName
     * @param address
     * @param zipCode
     * @param telephone
     * @param email
     */
    public Company(String companyName, String address, String zipCode, String telephone, String email) {

        this.companyName = companyName;
        this.address = address;
        this.zipCode = zipCode;
        this.telephone = telephone;
        this.email = email;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
