package org.hillel_Elem.homeworkreports;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.base.expression.AbstractSimpleExpression;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.HorizontalListBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.subtotal.AggregationSubtotalBuilder;
import net.sf.dynamicreports.report.constant.Calculation;
import net.sf.dynamicreports.report.definition.ReportParameters;
import java.math.BigDecimal;
import static net.sf.dynamicreports.report.builder.DynamicReports.*;

public class DesignReport {

    private DataReport data = new DataReport();

    private AggregationSubtotalBuilder<BigDecimal> totalSum;

    public DesignReport(DataReport data) {

        this.data = data;
    }

    /**
     *
     * @return
     * @throws Exception
     */
    public JasperReportBuilder build() throws Exception {

        JasperReportBuilder report = report();

        TextColumnBuilder<String> descriptionColumn = col.column("Description", "description", type.stringType());
        TextColumnBuilder<Integer> quantityColumn = col.column("Quantity", "count", type.integerType());
        TextColumnBuilder<BigDecimal> priceColumn = col.column("Price", "price", type.bigDecimalType());

        //total
        TextColumnBuilder<BigDecimal> totalColumn = priceColumn.multiply(quantityColumn)
                .setTitle("Total")
                .setDataType(Templates.currencyType);

        //subTotal
                totalSum = sbt.sum(totalColumn);

        //Vat
        TextColumnBuilder<BigDecimal> vatSum = totalColumn.multiply(data.getInvoice().getTax())
                         .setDataType(Templates.currencyType);

        //grandSum
        TextColumnBuilder<BigDecimal> grandSum = totalColumn.subtract(vatSum)
                        .setDataType(Templates.currencyType);

        report
                .setColumnTitleStyle(Templates.columnTitleStyle)
                .highlightDetailEvenRows()
                .columns(
                        descriptionColumn, quantityColumn, priceColumn, totalColumn).setColumnStyle(Templates.columnStyle)

                .columnGrid(

                        descriptionColumn, quantityColumn, priceColumn,
                        grid.horizontalColumnGridList()
                                .newRow()
                                .add(totalColumn)
                                .newRow()
                                .add(vatSum)
                                .newRow()
                                .add(grandSum)
                )

                .subtotalsAtSummary(
                        sbt.aggregate(exp.text("Total sum"), priceColumn, Calculation.NOTHING).setStyle(Templates.atSumarryStyle),
                        totalSum.setStyle(Templates.atSumarryStyle),
                        sbt.aggregate(exp.text("Tax "+ data.getInvoice().getTax()*100 + "%"), priceColumn, Calculation.NOTHING).setStyle(Templates.atSumarryStyle),
                        sbt.sum(vatSum).setStyle(Templates.atSumarryStyle),
                        sbt.aggregate(exp.text("GRAND TOTAL"), priceColumn, Calculation.NOTHING).setStyle(Templates.atSumarryStyle),
                        sbt.sum(grandSum).setStyle(Templates.atSumarryStyle)
                )

                .pageHeader(
                        cmp.horizontalList().setStyle(stl.style(10)).setGap(10).add(
                                cmp.hListCell(createCustomerAttribute("Sender", data.getInvoice().getSender()).setStyle(Templates.headerStyle)),
                                cmp.hListCell(createCustomerAttribute("Receiver", data.getInvoice().getReceiver()).setStyle(Templates.headerStyle)
                                )
                        ),
                        cmp.filler().setStyle(stl.style().setTopBorder(stl.pen2Point())).setFixedHeight(10),
                        cmp.text("REASON FOR EXPORT: ").setStyle(Templates.boldStyle),
                        cmp.verticalGap(60),
                        cmp.filler().setStyle(stl.style().setTopBorder(stl.pen2Point())).setFixedHeight(10)

                )

                .title(
                        cmp.horizontalList()
                                .add(
                                        cmp.image("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/picture-3170057-1427988346.png").setFixedDimension(80, 80),
                                        cmp.text("PROFORMA INVOICE").setStyle(Templates.boldCenteredStyle))
                                .newRow(),
                        cmp.verticalGap(10),
                        cmp.text("Invoice NO: " + data.getInvoice().getId()).setStyle(Templates.invoiceComponent),
                        cmp.text("Account: " + data.getInvoice().getAccount()).setStyle(Templates.invoiceComponent),
                        cmp.text("Date : " + data.getInvoice().getInvoiceData()).setStyle(Templates.invoiceComponent),
                        cmp.verticalGap(10)
                )

                .summary(
                        cmp.text("Accounts Manager").setStyle(Templates.accontsStyle),
                        cmp.text("Daniel THOMSON").setStyle(Templates.managerStyle),
                        cmp.horizontalList().setStyle(stl.style(20)).setGap(10).add(
                                cmp.hListCell(cmp.text("Payment terms: 30 days").setRows(2).setStyle(Templates.sumaryStyle)),
                                cmp.hListCell(
                                        cmp.image("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/1437192909_vygoda-ot-pokupki-faksimile.jpg")
                                                .setFixedDimension(150, 80)
                                )
                        )
                )

                .pageFooter(cmp.text("Thanks you for your business!").setStyle(Templates.footerStyle))
                .setDataSource(data.createDataSource());


        return report;
    }


    /**
     *
     * @param label
     * @param company
     * @return
     */
    private VerticalListBuilder createCustomerAttribute(String label, Company company){

        HorizontalListBuilder list = cmp.horizontalList().setBaseStyle(stl.style(Templates.headerStyle));

        addCustomerAttribute(list, "Company Name", company.getCompanyName());
        addCustomerAttribute(list,"Zip Code", company.getZipCode());
        addCustomerAttribute(list, "Address", company.getAddress());
        addCustomerAttribute(list,  "Phone", company.getTelephone());
        addCustomerAttribute(list,  "Email", company.getEmail());
        return  cmp.verticalList(
                cmp.text(label).setStyle(Templates.attributeStyle),
                list);
    }

    /**
     *
     * @param list
     * @param label
     * @param value
     */
    private void  addCustomerAttribute(HorizontalListBuilder list, String label, String value){

        if(value != null){
            list.add(cmp.text(label + ":").setFixedColumns(10).setStyle(Templates.boldStyle), cmp.text(value).setStyle(Templates.italicStyle)).newRow();
        }
    }

    /**
     *
     */
    private class TotalPaymentExpression extends AbstractSimpleExpression<String> {

        private static final long serialversionUID = 1L;

        @Override
        public String evaluate(ReportParameters reportParameters) {

            BigDecimal total = reportParameters.getValue(totalSum);
            return "Total payment: " + Templates.currencyType.valueToString(total, reportParameters.getLocale());
        }
    }
}