package org.hillel_Elem.homeworkreports;



import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DataReport {

  private Invoice invoice;

    public DataReport() {

        invoice = createInvoice();
    }

    /**
     *
     * @return
     */
    public Invoice getInvoice() {
        return invoice;
    }

    /**
     *
     * @param description
     * @param count
     * @param price
     * @return
     */
    private Item createIteam(String description, Integer count, BigDecimal price){

        Item item = new Item();
        item.setDescription(description);
        item.setCount(count);
        item.setPrice(price);

        return item;
    }

    /**
     *
     * @param companyName
     * @param zipCode
     * @param address
     * @param telephone
     * @param email
     * @return
     */
    private Company createCustomer(String companyName, String zipCode,  String address, String telephone, String email){

        Company customer = new Company();
        customer.setCompanyName(companyName);
        customer.setZipCode(zipCode);
        customer.setAddress(address);
        customer.setTelephone(telephone);
        customer.setEmail(email);

        return customer;

    }

    /**
     *
     * @return
     */
    private Invoice createInvoice(){

        Invoice invoice = new Invoice();

        List<Item> listItem = new ArrayList<>();
        listItem.add(createIteam("Notebook", 5, new BigDecimal(3736)));
        listItem.add(createIteam("DVD", 4, new BigDecimal(8348)));
        listItem.add(createIteam("TV", 2, new BigDecimal(97345)));
        listItem.add(createIteam("Telephone", 53, new BigDecimal(3236)));
        listItem.add(createIteam("Book", 42, new BigDecimal(3448)));
        listItem.add(createIteam("Watch", 22, new BigDecimal(9735)));

        invoice.setInvoice(listItem);
        invoice.setReceiver(createCustomer("VUSO","02352","Kyiv, str.Holosiivska 62", "044698745", "contact@vuso.com"));
        invoice.setSender(createCustomer("Microsoft", "65650", "225 High Bridge New York", "01 222890850","contact@microsoft.com"));
        invoice.setId(4);
        invoice.setAccount("00013629002");
        invoice.setInvoiceData("01.01.2017");
        invoice.setTax(0.2);
        invoice.setInvoice(listItem);

        return invoice;

    }

    /**
     *
     * @return
     */
    public JRDataSource createDataSource(){

        return new JRBeanCollectionDataSource(invoice.getInvoice());
    }
}
