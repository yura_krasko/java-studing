package org.hillel_Elem.homeworkreports;

import java.math.BigDecimal;

public class Item {

    private String description;
    private Integer count;
    private BigDecimal price;

    public Item() {
    }

    public Item(String description, Integer count, BigDecimal price) {
        this.description = description;
        this.count = count;
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
