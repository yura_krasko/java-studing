package org.hillel_Elem.homework;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LineJupiterTest {

    Line line = new Line(1.2, 4.2, 0.6, .001);
    Line line1 = new Line(0.1, 0.1, 0.8, 0.63);
    Line line2 = new Line(00.2, 0.76, 2.7, 1.11);


    @Test
    public void getSlopeTest(){

        System.out.println("test case getSlope");
        //seccuss
        Assertions.assertEquals(-0.19966666666666666, line.getSlope());
        //arithmetic exception
        Assertions.assertEquals( 0, line1.getSlope());
        //fail
        Assertions.assertNotEquals(-2.839285714285714, line2.getSlope());
    }

    @Test
    public void getDistanceTest(){

        System.out.println("test case getDistance");
        //success
        Assertions.assertEquals(3.0592157491749417, line.getDistance());
        //success
        Assertions.assertNotEquals(0, line2.getDistance());
        //fail
        Assertions.assertNotEquals(0.17000000000000004, line1.getDistance());



    }

    @Test
    public void parallelToTest(){

        System.out.println("test case parallelTo");
        //success
        Assertions.assertEquals(false, line.parallelTo(line2));
        //arithmetic exception
        Assertions.assertEquals(true,line.parallelTo(line1));
        //fail
        Assertions.assertNotEquals(false, line2.parallelTo(line));


    }
}
