package org.hillel_Elem.homework;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class LineJunitTest {

    Line line = new Line(1.0, 3.0, 4.0, 0.6);
    Line line1 = new Line(1.5, 1.5, 4.0, 0.0005);
    Line line2 = new Line(0.0001, 0.11, 0.2, 0.001);


    @Test
    public void getSlopeTest(){

        System.out.println("Test case getSlope");
        //success
        assertEquals(-1.6999, line.getSlope(), .0001);
        //arithmectic exception
        assertEquals( -1.02, line1.getSlope(), 1.0);
        //fail
        assertEquals(-0.56, line2.getSlope(), 0.5);



    }

    @Test
    public void getDistanseTest(){

        System.out.println("Test case getDistance");

        //success
        assertEquals(4.0, line1.getDistance(), .01);
        //fail
        assertEquals(-4.472, line.getDistance(), .001);
        //succes
        assertEquals(0.0, line2.getDistance(), .23);

    }

    @Test
    public void parallelToTest(){

        System.out.println("Test case parallelTo");

        //success
        assertEquals(true, line.parallelTo(line));
        //arithmetic exception
        assertEquals(false, line1.parallelTo(line2));
        //fail
        assertEquals(true, line2.parallelTo(line));


    }
}
