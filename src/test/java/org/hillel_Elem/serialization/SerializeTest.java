package org.hillel_Elem.serialization;

import org.junit.Test;

import java.io.*;
import java.util.Calendar;
import java.util.Date;

public class SerializeTest {

    @Test
    public void testFirst(){

        User myDetails = new User("Alex", "Smith", 102825, new Date(Calendar.getInstance().getTimeInMillis()));

        try(FileOutputStream fileOut = new FileOutputStream("User.txt")){

            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            FileInputStream in = new FileInputStream("User.txt");
            ObjectInputStream fileIn = new ObjectInputStream(in);
            out.writeObject(myDetails);

            User deserialization = null;

            try {
                deserialization = (User) fileIn.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            System.out.println(deserialization.getFirstName());
            System.out.println(deserialization.getLastName());
            System.out.println(deserialization.getAccountNumber());
            System.out.println(deserialization.getDateOpened());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
