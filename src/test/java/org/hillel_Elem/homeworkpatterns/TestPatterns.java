package org.hillel_Elem.homeworkpatterns;


import org.hillel_Elem.homeworkpatterns.abstractfactory.AbstractFactory;
import org.hillel_Elem.homeworkpatterns.abstractfactory.Car;
import org.hillel_Elem.homeworkpatterns.builder.CarBuilder;
import org.hillel_Elem.homeworkpatterns.builder.CarDirector;
import org.hillel_Elem.homeworkpatterns.builder.GermanCarBuilder;
import org.hillel_Elem.homeworkpatterns.factory.Figure;
import org.hillel_Elem.homeworkpatterns.factory.FigureFactory;
import org.junit.Test;

public class TestPatterns {

    @Test
    public void testFactory(){

        FigureFactory figureFactory = new FigureFactory();
        Figure figure = figureFactory.getFigure("Circle");
        System.out.printf(figure.darw() + ",\tArea: %.2f%n",figure.getArea());
        Figure figure1 = figureFactory.getFigure("Cylinder");
        System.out.printf(figure1.darw() + ",\tArea: %.2f",figure1.getArea());

    }

    @Test
    public void testAbstractFactory(){

       System.out.println("German factory: ");
       org.hillel_Elem.homeworkpatterns.abstractfactory.Car car = new AbstractFactory().getFactory("German").getFactory("BMW").driving();
       Car car1 = new AbstractFactory().getFactory("German").getFactory("Audi").driving();
       System.out.println("\nJapan factory: ");
       Car car2 = new AbstractFactory().getFactory("Japan").getFactory("Toyota").driving();
       Car car3 = new AbstractFactory().getFactory("Japan").getFactory("Nissan").driving();


    }

    @Test
    public void testBilder(){

        CarBuilder carBuilder = new GermanCarBuilder();
        CarDirector carDirector = new CarDirector(carBuilder);
        carDirector.constructCar();
        Car car = (Car) carDirector.getCar();
        System.out.println(car);
    }
}
