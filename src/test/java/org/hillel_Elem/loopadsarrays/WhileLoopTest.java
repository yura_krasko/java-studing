package org.hillel_Elem.loopadsarrays;

import org.junit.Test;

public class WhileLoopTest {

    @Test
    public void whileLooptesting(){

        int x = 0;

        while (x <= 20){
            System.out.println(x);
            x++;
        }
    }
}
