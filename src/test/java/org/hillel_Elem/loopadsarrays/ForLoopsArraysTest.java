package org.hillel_Elem.loopadsarrays;

import org.junit.Test;

import java.lang.reflect.Array;
import java.util.Arrays;

import  static org.hillel_Elem.loopandarrays.ForLoppsAndArrays.*;

public class ForLoopsArraysTest {

    @Test
    public void checkEqualityOfTwoArrays (){

        int [] array1 = {2,5,7,9,11};
        int [] array2 = {2,5,7,8,11};
        int [] array3 = {2,5,7,9,11};


        if(equalityOfTwoArrays(array1, array2)){
            System.out.println("Two arrays are equals");
        }
        else{
            System.out.println("Two arrays are not  equals");
        }

        //equalityOfTwoArrays(array1, array3);
        Arrays.fill(array1, 0, 2, 103);

        for (int i = 0; i < array1.length; i++){
            System.out.println(array1[i]);
        }
        System.out.println();
        System.out.println(Arrays.binarySearch(array2, 11));
    }


    @Test
    public  void uniqueElements(){

        uniqueArray(new int[] {0, 3, 4, 5, 3,5});
        System.out.println();
        System.out.println();

        uniqueArray(new int[] {10,22,10,20,11,22});
    }
}
