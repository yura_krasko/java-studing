package org.hillel_Elem.nio;

import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.HashSet;
import java.util.Set;

public class NioTest {

    final static String FIRST_FILE = "/home/yura/IdeaProjects/hillel_Elem/newTest1.txt";
    final static String PROJECT_PATH = "/home/yura/IdeaProjects/hillel_Elem/";

    @Test
    public void copyTest(){

        Path pathSource = Paths.get(PROJECT_PATH, "paths.txt");

        File destFile = new File(FIRST_FILE);
        File sourceFile = new File(FIRST_FILE);

        try(FileOutputStream fos = new FileOutputStream(destFile);
            FileInputStream fis = new FileInputStream(sourceFile)){

            long noOfBytes = Files.copy(pathSource, fos);
            System.out.println(noOfBytes);

            Path destPath1 = Paths.get(PROJECT_PATH, "newTest2.txt");

            noOfBytes = Files.copy(fis, destPath1, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(noOfBytes);

            Path destFile2 = Paths.get(PROJECT_PATH, "newTest3.txt");
            Path target = Files.copy(pathSource, destFile2, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(target.getFileName());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void directory() throws IOException {

        Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rwxrwx--x");
        FileAttribute<Set<PosixFilePermission>> fileAttribute = PosixFilePermissions.asFileAttribute(perms);
        Path path = Paths.get(PROJECT_PATH, "Parent", "Child");

        Files.createDirectories(path, fileAttribute);

        Files.createTempDirectory(path, "Concretepage");
    }

    @Test
    public void file() throws IOException {

        Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rwxrwx--x");
        FileAttribute<Set<PosixFilePermission>> fileAttribute  =PosixFilePermissions.asFileAttribute(perms);
        Path path = Paths.get(PROJECT_PATH, "Parent", "file.txt");

        Files.createFile(path, fileAttribute);

        Set<PosixFilePermission> posixFilePermissions = new HashSet<>();
        posixFilePermissions.add(PosixFilePermission.OWNER_READ);
        FileAttribute<Set<PosixFilePermission>> fileAttribute1 = PosixFilePermissions.asFileAttribute(posixFilePermissions);
        Path path1 = Paths.get(PROJECT_PATH, "Parent", "fileNew2.txt");
        Files.createFile(path1, fileAttribute1);
    }
}
