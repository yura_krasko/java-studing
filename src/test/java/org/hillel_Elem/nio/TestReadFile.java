package org.hillel_Elem.nio;



import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestReadFile {

    final static String POM = "/home/yura/IdeaProjects/hillel_Elem/pom.xml";

    @Test
    public void testRead(){

        try(Stream<String> stream = Files.lines(Paths.get(POM))){

            stream.forEach(System.out::println);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSecondReader(){

        List<String> list = new ArrayList<>();

        try(Stream<String> stream = Files.lines(Paths.get(POM))){
            list = stream.filter(line -> line.startsWith("<"))
                    .map(String::toUpperCase)
                    .collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }

        list.forEach(System.out::println);
    }

    @Test
    public void testReference(){

        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);

        arrayList.forEach(TestReadFile::square);
    }

    public static void square(int num){
        System.out.println(Math.pow(num, 2));
    }

    @Test
    public void paths() throws IOException {

        Path path = Paths.get(NioTest.PROJECT_PATH, "paths.txt");
        String question = "To be or not to be?";
        Files.write(path, question.getBytes());

        Path path1 = Paths.get(NioTest.PROJECT_PATH, "paths.txt");
        System.out.println(path1.getFileName() + " " +
        path1.getName(0) + " " +
                path1.getNameCount() + " " +
                path1.subpath(0, 2) + " " +
                path1.getParent() + " " +
                path1.getRoot()
        );

        TestReadFile.convertPath();
    }

    private static  void convertPath(){

        Path relative = Paths.get("src/main/resources/diggerencs.png");
        System.out.println("Relation path: " + relative);
        Path absolute = relative.toAbsolutePath();
        System.out.println("Absolute paths: " + absolute);
    }
}
