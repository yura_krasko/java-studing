package org.hillel_Elem.homeworkmultithreading;

import org.hillel_Elem.homeworkmultithreading.tasksecond.Car;
import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CarTest {

    @Test
    public void testCar(){

        final Integer COUNT_CAR = 100;

        ExecutorService executor = Executors.newFixedThreadPool(COUNT_CAR);

        for (int i = 1; i <= COUNT_CAR ; i++) {

            Car car = new Car(i);
            try {
                System.out.println(executor.submit(car).get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        executor.shutdown();
    }

}
