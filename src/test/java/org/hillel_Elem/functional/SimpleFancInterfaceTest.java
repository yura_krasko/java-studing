package org.hillel_Elem.functional;


import org.junit.Test;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.*;

public class SimpleFancInterfaceTest {

    @Test
    public void test(){

        checkWork(new SimpleFuncInterface(){

            @Override
            public void doWork() {
                System.out.println("Do work in SimpleFun impl...");
            }
        });

        checkWork(() -> System.out.println("Do work in lamda exp impl...."));
    }

    public static void checkWork(SimpleFuncInterface simpleFuncInterface){
        simpleFuncInterface.doWork();
    }

    @Test
    public void predicateTest(){

        Predicate<Integer> isPositive = x -> x > 0;
        System.out.println(isPositive.test(5));
        System.out.println(isPositive.test(-7));

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        System.out.println("Print all numbers: ");

        eval(list, n -> true);

        System.out.println("Print even bumbers: ");
        eval(list, n -> n%2 ==0);

        System.out.println("Print numbers greater than 3:");
        eval(list, n -> n >3);
    }

    public static void eval(List<Integer> list, Predicate<Integer> predicate){

        for (Integer n : list){
            if (predicate.test(n)){
                System.out.println(n + " ");
            }
        }
    }


    @Test
    public void binaeyOperator(){

        BinaryOperator<Integer> multiply = (x, y) -> x * y;

        System.out.println(multiply.apply(3, 5));
        System.out.println(multiply.apply(10, -2));
    }


    @Test
    public void unaryOperator(){

        UnaryOperator<Integer> square = x -> x * x;

        System.out.println(square.apply(5));
    }


    @Test
    public  void function(){

        Function<Integer, String> convert = x -> String.valueOf(x) + " euro";

        System.out.println(convert.apply(5));
    }


    @Test
    public  void consumer(){

        Consumer<Integer> printer = x ->{
            Integer integer = new Integer(x + 5);
            System.out.println(integer);
            System.out.printf("%d euro", x);
        };

        printer.accept(600);
    }

    @Test
    public void java8forEachAndMap(){

        Map<String, Integer> items = new HashMap<>();
        items.put("A", 10);
        items.put("B", 20);
        items.put("C", 30);
        items.put("D", 40);
        items.put("E", 50);
        items.put("F", 60);

        //regular
        for (Map.Entry<String, Integer> entry : items.entrySet()){
            System.out.println("Item " + entry.getKey() + " Count " + entry.getValue());
        }

        System.out.println("---------------------------------------");
        //lamda(java 8)
        items.forEach((k, v) ->System.out.println("Item " + k + " Count " + v));

        System.out.println("---------------------------------------");
        items.forEach((k, v) -> {

            System.out.println("Item " + k + " Count " + v);
            if ("E".equals(k)){
                System.out.println("Hello E");
            }
        });
    }

    @Test
    public void java8ForEachAndList(){

        List<String> items = new ArrayList<>();
        items.add("A");
        items.add("B");
        items.add("C");
        items.add("D");
        items.add("EB");
        items.add("F");

        for (String iteam : items){
            System.out.println(iteam);
        }

        System.out.println("-------------------------------------------");

        items.forEach(item -> System.out.println(items));

        System.out.println("--------------------------------------------");

        items.forEach(item -> {
            if ("B".equals(item)){
            System.out.println(item);
        }
        });

        System.out.println("---------------------------------------------");

       items.forEach(System.out :: println);

        System.out.println("---------------------------------------------");

        items.stream()
                .filter(s-> s.contains("B"))
                .forEach(System.out::println);
    }
}
