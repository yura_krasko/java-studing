package org.hillel_Elem.exception;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.hillel_Elem.exeptions.CustomerService;
import org.hillel_Elem.exeptions.NameNotFoundExeption;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static  junit.framework.TestCase.fail;
import static org.hamcrest.CoreMatchers.is;
import static  org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.CoreMatchers.containsString;



@RunWith(HierarchicalContextRunner.class)
public class ExceptionTest {

    @Test(expected = ArithmeticException.class)
    public void testDivisionWithException(){

        int i = 1 / 0;
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void tempEmptyList(){

        new ArrayList<>().get(0);
    }


    public class ExceptionTest2{

        @Test
        public void testDivisionWithException(){

           try {
               int i = 1 / 0;
               fail();
           } catch (ArithmeticException e){
               assertThat(e.getMessage(), is("/ by zero"));
           }

        }

        @Test
        public void tempEmptyList(){

            try {
                new ArrayList<>().get(0);
                fail();
            } catch (IndexOutOfBoundsException e){
                assertThat(e.getMessage(), is("Index: 0, Size: 0"));
                System.out.println(e.getMessage());
            }
        }

        @Test
        public void testFail() {

            try {

                CustomerService customerService = new CustomerService();
                customerService.findByName("");
                //need to comment super(message) on NameNotFoundException!
                fail();
            } catch (NameNotFoundExeption e){

            }
        }
    }

    public class ExceptionTest3{

        @Rule
        public ExpectedException thrown = ExpectedException.none();

        @Test
        public void testDivisionWithException(){

            thrown.expect(ArithmeticException.class);
            thrown.expectMessage(containsString("/ by zero"));

            int i = 1 / 0;
        }

        @Test
        public void testNameNotFoundException() throws NameNotFoundExeption{

            //test type
            thrown.expect(NameNotFoundExeption.class);

            //test messaeg
            thrown.expectMessage(is("Name is empty!"));

            //test detail
            thrown.expect(hasProperty("errCode"));
            thrown.expect(hasProperty("errCode", is(666)));

            CustomerService cust = new CustomerService();
            cust.findByName("");
        }

    }



}
