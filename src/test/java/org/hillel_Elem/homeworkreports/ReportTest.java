package org.hillel_Elem.homeworkreports;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;

public class ReportTest {

    @Test
    public void reportTest(){

        DataReport data = new DataReport();

        DesignReport designReport = new DesignReport(data);

        try {
            JasperReportBuilder report = designReport.build();
            report.toPdf(new FileOutputStream(new File("/home/yura/IdeaProjects/hillel_Elem/HomeworkReport.pdf")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
