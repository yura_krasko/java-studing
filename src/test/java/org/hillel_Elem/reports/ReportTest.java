package org.hillel_Elem.reports;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.exception.DRException;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class ReportTest {

    @Test
    public void reportTest() throws Exception {

        InvoiceData invoiceData = new InvoiceData();

        InvoiceDesign invoiceDesign = new InvoiceDesign(invoiceData);


            try {
                JasperReportBuilder report = invoiceDesign.build();
                report.toPdf(new FileOutputStream(new File("/home/yura/IdeaProjects/hillel_Elem/report.pdf")));
            } catch (DRException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }


//        @Test
//    public void test() throws Exception {
//
//            ListDataReport listData = new ListDataReport();
//            ListDesigneReport listDesign = new ListDesigneReport (listData);
//
//                JasperReportBuilder report = listDesign.build();
//                report.toPdf(new FileOutputStream(new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/report.pdf")));
//
//
//        }


}
