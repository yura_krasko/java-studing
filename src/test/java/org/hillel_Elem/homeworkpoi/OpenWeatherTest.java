package org.hillel_Elem.homeworkpoi;


import org.junit.Test;

public class OpenWeatherTest {

    @Test
    public void test(){

        String url = "http://api.openweathermap.org/data/2.5/forecast/daily?q=Kiev,ua&appid=f05ec39774d2e574e7ed5dccf5481efe";
        ConnectionOpenWeather weather = new ConnectionOpenWeather();
        CreateExcel createExcel = new CreateExcel();
        createExcel.parseJsonAndWriteExcel(weather.getData(url));
    }
}
