package org.hillel_Elem.homeworkserialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.hillel_Elem.homeworkserialization.model.Item;
import org.hillel_Elem.homeworkserialization.model.Order;
import org.junit.Test;

import java.io.*;

public class TestJson {

    @Test
    public void testGson(){

        JsonData data = new JsonData();

        Gson gson = new GsonBuilder().create();
        String gsonDate = gson.toJson(data.orderJson());

        try(FileOutputStream out = new FileOutputStream("src/main/resources/json/testGson.json")) {

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(out);
            objectOutputStream.writeObject(gsonDate);

            FileInputStream in = new FileInputStream("src/main/resources/json/testGson.json");
            ObjectInputStream objectInputStream = new ObjectInputStream(in);

            Order rez = gson.fromJson((String) objectInputStream.readObject(), Order.class);

            System.out.println("ID: " + rez.getId());
            System.out.println("Order number: " + rez.getNumberOrder());
            System.out.println("Date order: " + rez.getDateOrder());
            System.out.print("Customer: " + rez.getUser().getFirstName());
            System.out.print(" " + rez.getUser().getLastName());
            System.out.print(", " + rez.getUser().getAge());
            System.out.println(", " + rez.getUser().getPhone());
            System.out.print("Address: " +rez.getAddress().getCity());
            System.out.print(", " + rez.getAddress().getStreetAddress());
            System.out.println(", " + rez.getAddress().getPostCode());

            for (Item item : rez.getIteams()){
                System.out.print("Item: ");
                System.out.print(item.getDescription());
                System.out.print(", " + item.getQuantity());
                System.out.println(", " + item.getPrice());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testJackson(){

        JsonData data = new JsonData();

        ObjectMapper mapper = new ObjectMapper();

        try(FileOutputStream out  = new FileOutputStream("src/main/resources/json/testJasckson.json")) {

            ObjectOutputStream fileOut = new ObjectOutputStream(out);
            mapper.writeValue(fileOut, data.orderJson());

            FileInputStream in = new FileInputStream("src/main/resources/json/testJasckson.json");
            ObjectInputStream fileIn = new ObjectInputStream(in);

            Order orderDes = mapper.readValue(fileIn, Order.class);

            System.out.println("ID: " + orderDes.getId());
            System.out.println("Order number: " + orderDes.getNumberOrder());
            System.out.println("Date order: " + orderDes.getDateOrder());
            System.out.print("Customer: " + orderDes.getUser().getFirstName());
            System.out.print(" " + orderDes.getUser().getLastName());
            System.out.print(", " + orderDes.getUser().getAge());
            System.out.println(", " + orderDes.getUser().getPhone());
            System.out.print("Address: " +orderDes.getAddress().getCity());
            System.out.print(", " + orderDes.getAddress().getStreetAddress());
            System.out.println(", " + orderDes.getAddress().getPostCode());

            for (Item item : orderDes.getIteams()){
                System.out.print("Item: ");
                System.out.print(item.getDescription());
                System.out.print(", " + item.getQuantity());
                System.out.println(", " + item.getPrice());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
