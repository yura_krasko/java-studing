package org.hillel_Elem.inheritance;

import org.junit.Test;

public class RetangleTest {

    @Test
    public void retangleTest(){

        Retangle r = new Retangle();


        System.out.println("Retangle: "
                + " side a = " + r.getSide()
                + " side b = " + r.getSideSecond()
                + " Area = " + r.getAreaRetan());

        Retangle r1 = new Retangle(2);


        System.out.println("Retangle: "
                + " side a = " + r1.getSide()
                + " side b = " + r1.getSideSecond()
                + " Area = " + r1.getAreaRetan());

        Retangle r2 = new Retangle(2,3);


        System.out.println("Retangle: "
                + " side a = " + r2.getSide()
                + " side b = " + r2.getSideSecond()
                + " Area = " + r2.getAreaRetan());

    }
}
