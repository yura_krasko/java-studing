package org.hillel_Elem.inheritance;

import org.junit.Test;

public class SquareTest {

    @Test
    public void area(){
        Square square = new Square();

        System.out.println("Square: "
                + " side " + square.getSide()
                + " area " + square.getArea()
        );

        Square s1 = new Square(3);

        System.out.println("Square: "
                + " side " + s1.getSide()
                + " area " + s1.getArea()

        );

    }
}
