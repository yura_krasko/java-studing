package org.hillel_Elem.inheritance;

import org.junit.Test;

public class PyramidaTest {

    @Test
    public void rezult(){

        Pyramida pr = new Pyramida();

        System.out.println(pr.getVolume());

        Pyramida pr1 = new Pyramida(2);
        System.out.println(pr1.getVolume());

        Pyramida pr2 = new Pyramida(2,3,4);
        System.out.println(pr2.getVolume());
    }
}
