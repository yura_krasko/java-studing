package org.hillel_Elem.inheritance;

import org.junit.Test;

public class TrapezeTest {

    @Test
    public void trapezeTest(){

        Trapeze t = new Trapeze();

        System.out.println("Trapeze:"
                + " side " + t.getSide()
                + " sideSecond " + t.getSideSecond()
                + " height " + t.getHeight()
                + " Area " + t.getAreaTrap()
        );

        Trapeze t1 = new Trapeze(2);

        System.out.println("Trapeze:"
                + " side " + t1.getSide()
                + " sideSecond " + t1.getSideSecond()
                + " height " + t1.getHeight()
                + " Area " + t1.getAreaTrap()
        );

        Trapeze t2 = new Trapeze(3, 4);

        System.out.println("Trapeze:"
                + " side " + t2.getSide()
                + " sideSecond " + t2.getSideSecond()
                + " height " + t2.getHeight()
                + " Area " + t2.getAreaTrap()
        );

        Trapeze t3 = new Trapeze(2, 3, 4);

        System.out.println("Trapeze:"
                + " side " + t3.getSide()
                + " sideSecond " + t3.getSideSecond()
                + " height " + t3.getHeight()
                + " Area " + t3.getAreaTrap()
        );
    }
}
