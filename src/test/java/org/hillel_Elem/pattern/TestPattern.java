package org.hillel_Elem.pattern;


import org.hillel_Elem.pattern.abstractfactory.AbstractFactory;
import org.hillel_Elem.pattern.abstractfactory.SpeciesFactory;
import org.hillel_Elem.pattern.builder.*;
import org.hillel_Elem.pattern.factory.Animal;
import org.hillel_Elem.pattern.factory.AnimalFactory;
import org.hillel_Elem.pattern.prototype.Dog;
import org.hillel_Elem.pattern.prototype.Person;
import org.hillel_Elem.pattern.singleton.SingletonExemple;
import org.junit.Test;

import java.util.Arrays;

public class TestPattern {

    @Test
    public void factory() throws Exception{

        AnimalFactory animalFactory = new AnimalFactory();
        Animal a1 = animalFactory.getAnimal("Feline");
        System.out.println("a1 sound: " + a1.makeSound());
        Animal a2 = animalFactory.getAnimal("Canine");
        System.out.println("a2 sound: " + a2.makeSound());
    }

    @Test
    public void abstractFactory(){

        AbstractFactory abstractFactory = new AbstractFactory();
        SpeciesFactory speciesFactory = abstractFactory.getSpeciesFactory("reptile");

        Animal a1 = speciesFactory.getAnimal("tyrannosaurus");
        System.out.println("a1 sounf: " + a1.makeSound());

        Animal a2 = speciesFactory.getAnimal("snake");
        System.out.println("a2 sound: " + a2.makeSound());

        SpeciesFactory speciesFactory1 = abstractFactory.getSpeciesFactory("mammal");

        Animal a3 = speciesFactory1.getAnimal("dog");
        System.out.println("a3 sound: " + a3.makeSound());

        Animal a4 = speciesFactory1.getAnimal("cat");
        System.out.println("a4 sound : " + a4.makeSound());

        Animal animal = new AbstractFactory().getSpeciesFactory("mammal").getAnimal("dog");
        System.out.println(animal.makeSound());

    }

    @Test
    public void singleton(){

        SingletonExemple singletonExemple = SingletonExemple.getInstance();
        singletonExemple.sayHello();

        SingletonExemple singletonExemple1 = SingletonExemple.getInstance();
        singletonExemple1.sayHello();
    }

    @Test
    public void prototype(){

        Person person = new Person("Ferd");
        System.out.println("Person : " + person);

        Person person1 = (Person) person.doClone();
        System.out.println("Person 1 : " + person1);

        Dog dog = new Dog("Woof");
        System.out.println("Dog : " + dog);
        Dog dog1 = (Dog) dog.doClone();
        System.out.println("Dog 1 : " + dog1);
    }

    @Test
    public void builder2(){

        MealBuilder mealBuilder = new ItalianMealBuilder();
        MealDirector mealDirector= new MealDirector(mealBuilder);
        mealDirector.constructMeal();
        Meal meal  =mealDirector.getMeal();
        System.out.println("meal is: " + meal);

        mealBuilder = new JapaneseMealBuilder();
        mealDirector = new MealDirector(mealBuilder);
        mealDirector.constructMeal();
        meal = mealDirector.getMeal();
        System.out.println("meal is: "+meal);

        Meal x = new MealDirector(new JapaneseMealBuilder()).constructMeal().getMeal();
        System.out.println(x);
    }

    @Test
    public void build(){

        Student s = new Student.Builder().age(25).language(Arrays.asList("ukraine", "english")).name("Yura").build();

        System.out.println(s);

    }
}
