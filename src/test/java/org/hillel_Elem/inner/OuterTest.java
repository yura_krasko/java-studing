package org.hillel_Elem.inner;

import org.junit.Test;

public class OuterTest {

    public static int x;
    static {
        x = 3;
    }


    @Test
    public void outertest(){

        Outer.Nested nested = new Outer.Nested();
        nested.printNestedtext();

        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner();
        inner.printText();
        outer.doIt();


        Outer outer1 = new Outer(){

            @Override
            public void doIt(){

                System.out.println("Anonymous class doIt()");
            }
        };

        outer1.doIt();

    }



    @Test
    public void mySecondTest(){

        final String TEXT_TO_PRINT = "Text...";

        MyInterface myInterface = new MyInterface() {

            private  String text;

            {this.text = TEXT_TO_PRINT;}

            @Override
            public void doIt() {

                System.out.println("Anonymous class doIt()");
                System.out.println(this.text);

            }
        };

        myInterface.doIt();
    }



    @Test
    public void cacheTest(){

        String key = "Password";
        Integer pass = 123456;

        Cache cache = new Cache();
        cache.store(key, pass);

        System.out.println(cache.get("Password"));
        System.out.println(cache.getData("Password"));
    }

    
    @Test
    public void localInnertest(){

        LocalInner localInner = new LocalInner();
        localInner.display();
    }

}
