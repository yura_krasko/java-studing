package org.hillel_Elem.inner;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.*;
import org.junit.runner.RunWith;

@RunWith(HierarchicalContextRunner.class)
public class NestedTest {

    @BeforeClass
    public static void beforeAlltestMetods(){

        System.out.println("Invoked once before all test methods");
    }

    @Before
    public  void beforeEachTestMethods(){

        System.out.println("Invoked before each test methods");
    }

    @After
    public void afterEachTestMethods(){

        System.out.println("Invoke after each test metods");
    }

    @AfterClass
    public  static void afterAllTestMethods(){

        System.out.println("Invoke once after all test methods");
    }

    @Test
    public void rootClassTest(){

        System.out.println("Root class test");
    }

    public class ContextA{

        @Before
        public void beforeEachTestMethodOfContextA(){

            System.out.println("Invoked before each test method of context A");
        }

        @After
        public void afterEachTestMethodsOfContextA(){

            System.out.println("Invoked after test method of context A");
        }

        @Test
        public void contexATest(){

            System.out.println("Context A test");
        }

        public class ContextC{

            @Before
            public void beforeEachTestMethodOfContextxC(){

                System.out.println("Invoked before each test method of context C");
            }

            @After
            public void afterEachtestMethodOfContextC(){

                System.out.println("Invoked after test method of context C");
            }

            @Test
            public void contextCTest(){

                System.out.println("Context C test");
            }
        }
    }
}
