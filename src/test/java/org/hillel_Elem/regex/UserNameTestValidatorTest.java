package org.hillel_Elem.regex;

import org.hillel_Elem.reqex.UserNamevalidator;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class UserNameTestValidatorTest {

    private UserNamevalidator userNamevalidator;

    @BeforeClass
    public void initData(){
        userNamevalidator = new UserNamevalidator();
    }
    @DataProvider
    public Object[][] validUserNameProvider() {

        return new Object[][] {
            new String[]{"ale", "alex_2002", "alex_2002", "alex3-4_good"}

    };
    }

    @DataProvider
    public Object[][] invalidUserNameProvider(){

    return new Object[][]{{new String[]{"al", "al@exsey", "alexey12345_-"}}
    };
    }

    @Test(dataProvider = "validUserNameProvider")
    public void validUsernameTest(String[] userName){

        for (String temp : userName){
            boolean valid = userNamevalidator.validate(temp);
            System.out.println("Username is valid : " + temp + " , " + valid);
            Assert.assertEquals(true, valid);
        }
    }

    @Test(dataProvider = "invalidUserNameProvider",
                        dependsOnMethods = "validUsernameTest")
    public void inValidUsernameTest(String[] Username){

        for (String temp : Username){
            boolean valid = userNamevalidator.validate(temp);
            System.out.println("Username is valid : " + temp + " , " + valid);
            Assert.assertEquals(false, valid);
        }
    }
}
