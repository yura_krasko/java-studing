package org.hillel_Elem.polymorphism;

import org.junit.Test;

import java.util.Collections;

import static org.hillel_Elem.polymorphism.StaffMember.*;

public class PolimorphismTest {

    /**
     *
     */
    @Test
    public void printPayDayTest(){

        Staff st = new Staff();

           st.payday();

    }

    /**
     *
     */
    @Test
    public void sortByPayTest(){

        Staff st = new Staff();

     Collections.sort(st.staffList, payComparator);
     st.payday();

    }

    /**
     *
     */
    @Test
    public void sortByPayConsultationTest(){

        Staff st = new Staff();

        Collections.sort(st.staffList, payConsComparator);
        st.payday();
    }

    /**
     *
     */
    @Test
    public void sortByPayDocumentationTest(){

        Staff st = new Staff();

        Collections.sort(st.staffList, payDocComparator);
        st.payday();
    }


    /**
     *
     */
    @Test
    public void sortByPayDevelopmentTest(){

        Staff st = new Staff();

        Collections.sort(st.staffList, payDevComparator);
        st.payday();
    }
}
