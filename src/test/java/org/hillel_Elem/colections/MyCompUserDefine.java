package org.hillel_Elem.colections;

import org.junit.Test;

import java.util.TreeSet;

public class MyCompUserDefine {

    @Test
    public void test(){

        TreeSet<Empl> nameComp = new TreeSet<>(new MyNameComp());
        nameComp.add(new Empl("Ram", 3000));
        nameComp.add(new Empl("John", 5000));
        nameComp.add(new Empl("Yura", 10000));
        nameComp.add(new Empl("Tom", 6000));
        for (Empl e : nameComp){

            System.out.println(e);
        }

        System.out.println("==============================");
        nameComp.forEach(e ->{
            System.out.println(e);
        });

        System.out.println("==============================");
        TreeSet<Empl> salaryComp = new TreeSet<>(new MySalaryComp());
        salaryComp.add(new Empl("Raw", 3000));
        salaryComp.add(new Empl("Tom", 5000));
        salaryComp.add(new Empl("Yura", 10000));
        salaryComp.add(new Empl("John", 1000));
        for (Empl s : salaryComp){

            System.out.println(s);
        }
    }
}
