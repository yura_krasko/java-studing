package org.hillel_Elem.colections;


import org.junit.Test;

public class MyFruitIteratorExample {

    @Test
    public void test(){

        MyOwnArrayList<String> fruitsList = new MyOwnArrayList<>();
        fruitsList.add("Mango");
        fruitsList.add("Strawaberry");
        fruitsList.add("Papaya");
        fruitsList.add("Watermalon");

        for (int i = 0; i < fruitsList.size(); i++){

            System.out.println(fruitsList.get(i));
        }

        System.out.println("---------Calling my oterator on my ArrayList----------");

        FruitIterator it = fruitsList.iterator();
        while (it.hasNext()){

            String s = (String) it.next();
            System.out.println("Value: " + s);
        }

        System.out.println("----Fruit List size: " + fruitsList.size());
        fruitsList.remove(1);
        System.out.println("----After removal, Fruit List size: " + fruitsList.size());

        for (int i = 0; i < fruitsList.size(); i++){

            System.out.println(fruitsList.get(i));
        }

        MyOwnArrayList<Integer> myOwnArrayList = new MyOwnArrayList<>();
        myOwnArrayList.add(1);
        myOwnArrayList.add(2);
        myOwnArrayList.add(3);
        for (int i = 0; i < myOwnArrayList.size(); i++){

            System.out.println(myOwnArrayList.get(i));
        }
    }
}
