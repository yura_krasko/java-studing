package org.hillel_Elem.interfaces;

import org.junit.Test;

import java.util.*;

public class SortFruitObject {

    @Test
    public void sortFruitObject(){

    List<Fruit> fruitList = new ArrayList<>();

    Fruit pineapple = new Fruit("Pineapple", "Pineapple description", 70);
    Fruit apple = new Fruit("Apple", "Apple discroption", 100);
    Fruit orange = new Fruit("Orange", "Orange descrription", 80);
    Fruit banana = new Fruit("Banaan", "Banana descripption", 90);
    Fruit apple2= new Fruit("Apple", "Apple description", 110);

    fruitList.add(pineapple);
    fruitList.add(apple);
    fruitList.add(orange);
    fruitList.add(banana);
    fruitList.add(apple2);

    Collections.sort(fruitList);

//    for(Fruit fruit : fruitList){
//        System.out.println(fruit.getFruitName() + " " + fruit.getQuantity());
//    }

    Collections.sort(fruitList, Fruit.fruitNameComparator);
        for (Fruit temp : fruitList){

            System.out.println("fruits " + " : " + temp.getFruitName() +
            ", Quantity : " + temp.getQuantity()
            );
        }


    }
}
