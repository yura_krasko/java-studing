package org.hillel_Elem.generics;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Testgen {

    @Test
    public void test(){

        GenericSimpl<String> genericSimpl = new GenericSimpl<>();
        genericSimpl.test("test");

        GenericSimpl<Integer> genericSimpl1 = new GenericSimpl<>();
        genericSimpl1.test(5);
    }

    @Test
    public void test2(){

        List<Integer> list = new ArrayList<>();
        list.add(5);

        List<Dog> dogList = new ArrayList<>();
        dogList.add(new Dog().name("Sharic"));

        List<Animal> animals = new ArrayList<>();

        Animal animal = new Animal() {

            String name;
            @Override
            Animal name(Object o) {
                this.name = (String) o;
                return this;
            }

            @Override
            Object getData() {
                return this.name;
            }

            @Override
            public String toString() {
                return "$classname{" +
                        "name='" + name + '\'' +
                        '}';
            }
        };

        animal.name("Zhiraf");
        animals.add(animal);

        collection1(list);
        collection2(dogList);
        collection3(animals);
    }

    public void collection1(List<?> list) {
        list.forEach(System.out::println);
    }

    public void collection2(List<? extends  Animal> list) {
        list.forEach(x -> System.out.println(x.getData()));
    }

    public void collection3(List<? super Dog> list) {
        list.forEach(x -> System.out.println(x.toString()));
    }
}
