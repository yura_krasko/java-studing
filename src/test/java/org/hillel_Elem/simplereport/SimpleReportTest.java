package org.hillel_Elem.simplereport;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import org.hillel_Elem.simplereports.ListDataReport;
import org.hillel_Elem.simplereports.ListDesigneReport;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;

public class SimpleReportTest {
    @Test
    public void test() throws Exception {

        ListDataReport listData = new ListDataReport();
        ListDesigneReport listDesign = new ListDesigneReport (listData);

        JasperReportBuilder report = listDesign.build();
       report.toPdf(new FileOutputStream(new File("/home/yura/IdeaProjects/hillel_Elem/src/main/resources/report.pdf")));


    }

}
