package org.hillel_Elem.junit;


import org.junit.jupiter.api.*;

import java.util.function.Supplier;


public class AppTest {

    @BeforeAll
    static  void setup(){

        System.out.println("@BeforeAll executed");
    }

    @BeforeEach
    void setupThis(){

        System.out.println("@BeforeEach executed");
    }

    @Tag("DEV")
    @Test
    void testCalcOne(){

        System.out.println("======TEST ONE EXECUTED======");
        Assertions.assertEquals(4, DataMethods.sum(2, 2));
    }

    @Tag("PROD")
    @Disabled
    @Test
    void testCalcTwo(){

        System.out.println("======TEST ONE EXECUTED=======");
    }

    @AfterEach
    void tearThis(){

        System.out.println("@AfetrEach executed");
    }

    @AfterAll
    static void tear(){

        System.out.println("@AfterAll execute");
    }

    @Tag("DEV")
    @Test
    void testCase(){

        Assertions.assertNotEquals(3, DataMethods.sum(2, 2));

        Assertions.assertNotEquals(5, DataMethods.sum(2, 2));

        Supplier<String> messageSupplier = () -> "DataMethodstest.Sum(2, 2) test failed";

        Assertions.assertNotEquals(5, DataMethods.sum(2, 2), messageSupplier);
    }
}
