package org.hillel_Elem.junit;

import org.junit.Test;

import javax.xml.crypto.Data;

import static junit.framework.TestCase.assertEquals;

public class DataMethodsTest {

    @Test
    public void testFindmax(){

        assertEquals(4, DataMethods.findMax(new int[]{1, 3, 4, 2}));
        assertEquals(-1, DataMethods.findMax(new int[]{-12,-1, -3, -4, -2}));
    }

    @Test
    public void multiplacationOfZeroIntegersShouldReturnZero (){

        DataMethods dataMethods = new DataMethods();

        assertEquals("10 x 0 must be 0", 0, dataMethods.multiply(10, 0));
        assertEquals("0 x 10 must be 0", 0, dataMethods.multiply(0, 10));
        assertEquals("0 x 0 must be 0", 0 , dataMethods.multiply(0, 0));
    }
}
