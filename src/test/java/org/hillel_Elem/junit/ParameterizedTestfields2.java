package org.hillel_Elem.junit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertEquals;

@RunWith(Parameterized.class)
public class ParameterizedTestfields2 {

    private int m1;
    private int m2;

    public ParameterizedTestfields2(int p1, int p2){

        m1 =p1;
        m2 = p2;
    }

    @Parameterized.Parameters
    public static Collection<Object []> data(){

        Object[][] data = new Object[][]{{1, 2}, {5, 3}, {121, 4}};
        return Arrays.asList(data);
    }

    @Test
    public void testMultiplyExeption(){

        DataMethods dataMethods = new DataMethods();

        assertEquals("Result", m1 * m2, dataMethods.multiply(m1, m2));
    }
}
