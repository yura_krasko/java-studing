package org.hillel_Elem.junit;

import org.junit.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import static junit.framework.TestCase.assertEquals;

public class DataMethodsTest2 {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception{

        System.out.println("before class");

//        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("test.txt")));
//        bufferedReader.read();
    }

    @Before
    public  void setUp(){
        System.out.println("before");
    }

    @Test
    public void testFindMax(){

        System.out.println("test case find max");

        assertEquals(4, DataMethods.findMax(new int[]{1, 3, 4,2} ));
    }

    @Test
    public void testCube(){

        System.out.println("test case cube");
        assertEquals(27, DataMethods.cube(3));
    }

    @Test
    public void testReverseWord(){

        System.out.println("test case reverse word");
        assertEquals("ym eman si nahk ", DataMethods.reverseWork("my name is khan"));
    }

    @After
    public void teamDown() throws Exception{

        System.out.println("after");
    }

    @AfterClass
    public static void teamDownAfterClass() throws Exception{

        System.out.println("after class");

    }
}
